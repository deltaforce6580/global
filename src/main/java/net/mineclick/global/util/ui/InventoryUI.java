package net.mineclick.global.util.ui;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import net.mineclick.global.GlobalPlugin;
import net.mineclick.global.service.PlayersService;
import net.mineclick.global.util.Runner;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import java.util.stream.Collectors;

public class InventoryUI {
    @Getter
    private static final CopyOnWriteArraySet<InventoryUI> activeUIs = new CopyOnWriteArraySet<>();

    private final String title;
    private final int size;
    private final Map<Integer, ItemUI> items = new HashMap<>();
    private final Map<Player, Inventory> associatedPlayers = new HashMap<>();
    private final Set<ClickType> allowedClickTypes = new HashSet<>(Collections.singletonList(ClickType.LEFT));
    private final BukkitTask task;
    private final Map<Runnable, AtomicInteger> scheduledTasks = new ConcurrentHashMap<>();
    private boolean destroyOnClose = true;
    @Setter
    @Getter
    private int updatePeriod = 0;
    private int currentPeriod = 0;

    public InventoryUI(String title, int size) {
        this.title = title;
        this.size = size;

        task = new BukkitRunnable() {
            @Override
            public void run() {
                // Scheduled tasks
                scheduledTasks.entrySet().removeIf(entry -> {
                    if (entry.getValue().decrementAndGet() <= 0) {
                        try {
                            entry.getKey().run();
                        } catch (Exception e) {
                            GlobalPlugin.i().getLogger().log(Level.SEVERE, "InventoryUI scheduled task threw an exception", e);
                        }
                        return true;
                    }
                    return false;
                });

                if (currentPeriod++ < updatePeriod) return;
                currentPeriod = 0;

                associatedPlayers.entrySet().removeIf(e -> !e.getKey().getOpenInventory().getTopInventory().equals(e.getValue()));
                if (!associatedPlayers.isEmpty()) {
                    items.values().stream().filter(i -> i.getUpdateConsumer() != null).forEach(i -> {
                        try {
                            i.getUpdateConsumer().accept(i);
                        } catch (Exception e) {
                            GlobalPlugin.i().getLogger().log(Level.SEVERE, "InventoryUI item update task threw an exception", e);
                        }
                    });
                    Set<Map.Entry<Integer, ItemUI>> entries = items.entrySet().stream().filter(e -> e.getValue().hasChanged()).collect(Collectors.toSet());
                    if (!entries.isEmpty()) {
                        entries.forEach(e -> associatedPlayers.values().forEach(i -> i.setItem(e.getKey(), e.getValue().getItem())));
                    }
                }
            }
        }.runTaskTimer(GlobalPlugin.i(), 0, 1);
    }

    public void addAllowedClickType(ClickType... types) {
        allowedClickTypes.addAll(Arrays.asList(types));
    }

    public InventoryUI setDestroyOnClose(boolean set) {
        destroyOnClose = set;
        return this;
    }

    public InventoryUI setItem(int position, @NonNull ItemUI itemUI) {
        items.put(position, itemUI);

        return this;
    }

    public ItemUI getItem(int position) {
        return items.get(position);
    }

    public void open(Player player) {
        Inventory inventory = Bukkit.createInventory(player, size, title);
        items.forEach((key, value) -> inventory.setItem(key, value.getItem()));

        schedule(1, () -> {
            associatedPlayers.put(player, inventory);
            player.openInventory(inventory);
        });
        activeUIs.add(this);
    }

    public boolean hasAssociated(Player player) {
        Inventory inventory = player.getOpenInventory().getTopInventory();
        Inventory has = associatedPlayers.get(player);

        return has != null && has.equals(inventory);
    }

    public void destroy() {
        associatedPlayers.entrySet().stream()
                .filter(e -> e.getKey().getOpenInventory().getTopInventory().equals(e.getValue()))
                .forEach(e -> e.getKey().closeInventory());
        associatedPlayers.clear();

        if (task != null)
            task.cancel();

        activeUIs.remove(this);
    }

    public void onClick(InventoryClickEvent e) {
        ItemUI itemUI = items.get(e.getRawSlot());
        if (itemUI != null && itemUI.getClickConsumer() != null) {
            e.setCancelled(true);
            if (allowedClickTypes.contains(e.getClick())) {
                PlayersService.i().get(e.getWhoClicked().getUniqueId(),
                        playerModel -> itemUI.getClickConsumer().accept(new InventoryClickPack(playerModel, e, itemUI)));
            }
        }
    }

    public void onClose() {
        Runner.sync(() -> {
            associatedPlayers.entrySet().removeIf(e -> !e.getKey().getOpenInventory().getTopInventory().equals(e.getValue()));
            if (associatedPlayers.isEmpty() && destroyOnClose)
                destroy();
        });
    }

    public void schedule(int delay, Runnable task) {
        scheduledTasks.put(task, new AtomicInteger(delay));
    }
}
