package net.mineclick.global.util;

import lombok.Getter;
import net.mineclick.global.GlobalPlugin;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.function.Consumer;

public class Runner {
    public static void sync(Runnable runnable) {
        new BukkitRunnable() {
            @Override
            public void run() {
                runnable.run();
            }
        }.runTask(GlobalPlugin.i());
    }

    public static void sync(int delay, Runnable runnable) {
        new BukkitRunnable() {
            @Override
            public void run() {
                runnable.run();
            }
        }.runTaskLater(GlobalPlugin.i(), delay);
    }

    public static void sync(int delay, int period, Consumer<State> consumer) {
        State state = new State();
        new BukkitRunnable() {
            @Override
            public void run() {
                if (state.canceled) {
                    cancel();
                } else {
                    consumer.accept(state);
                    state.tick();
                }
            }
        }.runTaskTimer(GlobalPlugin.i(), delay, period);
    }

    public static void async(Runnable runnable) {
        new BukkitRunnable() {
            @Override
            public void run() {
                runnable.run();
            }
        }.runTaskAsynchronously(GlobalPlugin.i());
    }

    public static void async(int delay, Runnable runnable) {
        new BukkitRunnable() {
            @Override
            public void run() {
                runnable.run();
            }
        }.runTaskLaterAsynchronously(GlobalPlugin.i(), delay);
    }

    public static void async(int delay, int period, Consumer<State> consumer) {
        State state = new State();
        new BukkitRunnable() {
            @Override
            public void run() {
                if (state.canceled) {
                    cancel();
                } else {
                    consumer.accept(state);
                    state.tick();
                }
            }
        }.runTaskTimerAsynchronously(GlobalPlugin.i(), delay, period);
    }

    @Getter
    public static class State {
        private long ticks = 0;
        private boolean canceled;

        public void cancel() {
            canceled = true;
        }

        private void tick() {
            ticks++;
        }
    }
}
