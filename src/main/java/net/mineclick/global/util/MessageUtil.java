package net.mineclick.global.util;

import net.kyori.adventure.text.Component;
import net.kyori.adventure.title.Title;
import net.kyori.adventure.title.TitlePart;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.mineclick.global.model.PlayerModel;
import org.bukkit.entity.Player;

import java.time.Duration;

public class MessageUtil {
    public static void sendHotbar(String msg, PlayerModel... players) {
        for (PlayerModel player : players) {
            player.getPlayer().spigot().sendMessage(net.md_5.bungee.api.ChatMessageType.ACTION_BAR, new ComponentBuilder(msg).create());
        }
    }

    public static void sendTitle(String title, String subtitle, PlayerModel... players) {
        for (PlayerModel player : players) {
            Player pl = player.getPlayer();
            if (pl != null) {
                pl.sendTitlePart(TitlePart.TITLE, Component.text(title));
                pl.sendTitlePart(TitlePart.SUBTITLE, Component.text(subtitle));
                pl.sendTitlePart(TitlePart.TIMES, Title.Times.of(Duration.ZERO, Duration.ofMillis(1250), Duration.ofMillis(250)));
            }
        }
    }
}
