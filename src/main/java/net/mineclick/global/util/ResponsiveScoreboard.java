package net.mineclick.global.util;

import lombok.NonNull;
import net.mineclick.global.model.PlayerModel;
import net.mineclick.global.type.Rank;
import net.minecraft.network.protocol.game.ClientboundSetScorePacket;
import net.minecraft.server.ServerScoreboard;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

public class ResponsiveScoreboard {
    private final static Scoreboard scoreboard;
    private final static Objective objective;
    private final static Map<Rank, Team> teams = new HashMap<>();

    static {
        scoreboard = Bukkit.getScoreboardManager().getNewScoreboard();
        objective = scoreboard.registerNewObjective("mc", "dummy", "mc");
        objective.setDisplaySlot(DisplaySlot.SIDEBAR);
        objective.setDisplayName(ChatColor.GRAY + ChatColor.BOLD.toString() + "MINECLICK.NET");

        for (Rank rank : Rank.values()) {
            Team team = scoreboard.registerNewTeam(rank.getName());
            team.setOption(Team.Option.COLLISION_RULE, Team.OptionStatus.NEVER);
            team.setOption(Team.Option.NAME_TAG_VISIBILITY, Team.OptionStatus.ALWAYS);
            team.setPrefix(rank.getPrefix());
            teams.put(rank, team);
        }
    }

    private final PlayerModel playerModel;
    private final Map<Integer, String> scoreCache = new HashMap<>();

    public ResponsiveScoreboard(PlayerModel playerModel) {
        this.playerModel = playerModel;
        Player player = playerModel.getPlayer();

        player.setScoreboard(scoreboard);
        player.setCollidable(false);
        teams.get(playerModel.getRank()).addEntry(playerModel.getName());
    }

    public void delete() {
        teams.get(playerModel.getRank()).removeEntry(playerModel.getName());
    }

    public void setScore(int index, @NonNull String score) {
        removeScore(index);
        scoreCache.put(index, score);

        ClientboundSetScorePacket packet = new ClientboundSetScorePacket(ServerScoreboard.Method.CHANGE, objective.getName(), getIndex(index) + score, 0);
        playerModel.sendPacket(packet);
    }

    public void removeAll() {
        new HashSet<>(scoreCache.keySet()).forEach(this::removeScore);
    }

    public void removeScore(int index) {
        String score = scoreCache.remove(index);
        if (score == null) return;

        ClientboundSetScorePacket packet = new ClientboundSetScorePacket(ServerScoreboard.Method.REMOVE, objective.getName(), getIndex(index) + score, 0);
        playerModel.sendPacket(packet);
    }

    private String getIndex(int index) {
        if (index >= 16) {
            return ChatColor.WHITE.toString();
        }

        return ChatColor.values()[index].toString();
    }
}
