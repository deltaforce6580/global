package net.mineclick.global.util;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class IfNull {
    @Getter
    private boolean isNull;
    private boolean await;
    private Runnable runnable;

    public IfNull(boolean isNull) {
        this.isNull = isNull;
    }

    public void ifNull(Runnable runnable) {
        if (await) {
            this.runnable = runnable;
        } else {
            if (isNull) {
                runnable.run();
            }
        }
    }

    public void await() {
        await = true;
    }

    public void releaseAwait(boolean isNull) {
        this.isNull = isNull;
        if (runnable != null && isNull) {
            runnable.run();
        }
        runnable = null;
    }
}
