package net.mineclick.global.util;

import com.google.gson.reflect.TypeToken;
import net.mineclick.global.GlobalPlugin;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.function.Consumer;

public class UUIDFetcher {
    public static void getUUID(String name, Consumer<UUID> consumer) {
        Runner.async(() -> consumer.accept(getUUIDSync(name)));
    }

    public static UUID getUUIDSync(String name) {
        String response = callURL("https://api.mojang.com/users/profiles/minecraft/" + name);

        Map<String, String> map = GlobalPlugin.i().getGson().fromJson(response, new TypeToken<HashMap<String, String>>() {
        }.getType());
        if (map == null || map.isEmpty())
            return null;

        return UUID.fromString(map.get("id").replaceAll("(\\w{8})(\\w{4})(\\w{4})(\\w{4})(\\w{12})", "$1-$2-$3-$4-$5"));
    }

    private static String callURL(String URL) {
        StringBuilder builder = new StringBuilder();
        try {
            URL url = new URL(URL);
            URLConnection connection = url.openConnection();

            if (connection != null) {
                connection.setReadTimeout(10 * 1000);

                if (connection.getInputStream() != null) {
                    try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream(), Charset.defaultCharset()))) {
                        int c;
                        while ((c = bufferedReader.read()) != -1) {
                            builder.append((char) c);
                        }
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return builder.toString();
    }
}
