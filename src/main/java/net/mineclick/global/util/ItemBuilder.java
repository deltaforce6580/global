package net.mineclick.global.util;

import com.google.common.base.Preconditions;
import lombok.Builder;
import lombok.Singular;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.enchantments.EnchantmentTarget;
import org.bukkit.enchantments.EnchantmentWrapper;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;

import java.lang.reflect.Field;
import java.util.List;

@Builder
public class ItemBuilder {
    private String title;
    @Singular("lore")
    private List<String> lores;
    private Material material;
    private Color color;
    @Builder.Default
    private int amount = 1;
    private boolean glowing;
    private boolean unbreakable;
    private String skull;

    public static boolean isSameTitle(ItemStack stack, ItemStack other) {
        return stack.hasItemMeta() && other.hasItemMeta()
                && stack.getItemMeta().hasDisplayName() && other.getItemMeta().hasDisplayName()
                && stack.getItemMeta().getDisplayName().equals(other.getItemMeta().getDisplayName());
    }

    public ItemStack toItem() {
        if (skull != null) {
            material = Material.PLAYER_HEAD;
        }
        Preconditions.checkNotNull(material);

        ItemStack item = new ItemStack(material, amount);
        ItemMeta meta = item.getItemMeta();

        if (color != null && meta instanceof LeatherArmorMeta) {
            ((LeatherArmorMeta) meta).setColor(color);
        }

        if (meta != null) {
            if (title != null) {
                meta.setDisplayName(title);
            }
            if (lores != null) {
                meta.setLore(lores);
            }
            if (unbreakable) {
                meta.setUnbreakable(true);
            }
            meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES, ItemFlag.HIDE_ENCHANTS, ItemFlag.HIDE_POTION_EFFECTS);
            item.setItemMeta(meta);
        }

        if (glowing) {
            item.addEnchantment(GlowEnchant.get(), 1);
        }

        if (skull != null) {
            Skins.set(item, skull);
        }

        return item;
    }

    public static class GlowEnchant extends EnchantmentWrapper {
        private static Enchantment enchantment;

        GlowEnchant(String id) {
            super(id);
        }

        public static Enchantment get() {
            if (enchantment == null) {
                try {
                    Field field = Enchantment.class.getDeclaredField("acceptingNew");
                    field.setAccessible(true);
                    field.set(null, true);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                Enchantment.registerEnchantment(enchantment = new GlowEnchant("custom_glow"));
            }
            return enchantment;
        }

        public String getName() {
            return "GlowEnchant";
        }

        public EnchantmentTarget getItemTarget() {
            return null;
        }

        public boolean canEnchantItem(ItemStack item) {
            return true;
        }

        public boolean conflictsWith(Enchantment other) {
            return false;
        }

        public int getStartLevel() {
            return 1;
        }

        public int getMaxLevel() {
            return 2;
        }
    }
}
