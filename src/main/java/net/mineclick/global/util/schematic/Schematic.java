package net.mineclick.global.util.schematic;

import com.destroystokyo.paper.profile.PlayerProfile;
import com.destroystokyo.paper.profile.ProfileProperty;
import it.unimi.dsi.fastutil.shorts.ShortArraySet;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import net.mineclick.global.GlobalPlugin;
import net.mineclick.global.config.SaveConstructor;
import net.mineclick.global.model.PlayerModel;
import net.mineclick.global.util.Pair;
import net.mineclick.global.util.Skins;
import net.mineclick.global.util.location.Region;
import net.minecraft.core.BlockPos;
import net.minecraft.core.SectionPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.ListTag;
import net.minecraft.network.protocol.game.ClientboundBlockEntityDataPacket;
import net.minecraft.network.protocol.game.ClientboundSectionBlocksUpdatePacket;
import net.minecraft.world.level.block.entity.SkullBlockEntity;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.chunk.LevelChunkSection;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Skull;
import org.bukkit.block.data.BlockData;
import org.bukkit.craftbukkit.v1_20_R1.block.data.CraftBlockData;

import java.io.*;
import java.lang.reflect.Field;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.logging.Level;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

@Getter
@RequiredArgsConstructor
@SaveConstructor(args = "path")
public class Schematic {
    private final String path;
    private final Set<ClientboundSectionBlocksUpdatePacket> packets = new HashSet<>();
    private final Set<ClientboundBlockEntityDataPacket> tileEntityPackets = new HashSet<>();
    private boolean loaded;
    private boolean loading;

    @Override
    public String toString() {
        String[] split = path.split("/");
        return split.length > 0 ? split[split.length - 1] : path;
    }

    private byte[] compress(final String str) throws IOException {
        if ((str == null) || (str.isEmpty())) {
            return null;
        }
        ByteArrayOutputStream obj = new ByteArrayOutputStream();
        GZIPOutputStream gzip = new GZIPOutputStream(obj);
        gzip.write(str.getBytes(StandardCharsets.UTF_8));
        gzip.flush();
        gzip.close();
        return obj.toByteArray();
    }

    public void save(Region region) {
        ArrayList<String> blocks = new ArrayList<>();
        ArrayList<String> types = new ArrayList<>();
        ArrayList<String> textures = new ArrayList<>();
        region.forBlocks((x, y, z) -> {
            Block block = region.getWorld().getBlockAt(x, y, z);
            Material type = block.getType();
            String typeString = type.toString().toLowerCase();
            String blockString = block.getBlockData().getAsString().replace("minecraft:", "");

            // store types for economy
            int typeId = types.indexOf(typeString);
            if (typeId < 0) {
                types.add(typeString);
                typeId = types.size() - 1;
            }
            blockString = blockString.replace(typeString, String.valueOf(typeId));

            // pull out skull texture if any
            if (type.equals(Material.PLAYER_HEAD) || type.equals(Material.PLAYER_WALL_HEAD)) {
                String texture = null;
                Skull state = (Skull) block.getState();
                if (state.getPlayerProfile() != null) {
                    ProfileProperty profileProperty = state.getPlayerProfile().getProperties().stream().filter(p -> p.getName().equals("textures")).findFirst().orElse(null);
                    if (profileProperty != null) {
                        texture = profileProperty.getValue();
                    }
                }

                if (texture != null) {
                    int textureId = textures.indexOf(texture);
                    if (textureId < 0) {
                        textures.add(texture);
                        textureId = textures.size() - 1;
                    }

                    blockString = blockString + "?" + textureId;
                }
            }

            // save to the list of blocks
            blocks.add(blockString);
        });

        try {
            Region normalized = region.normalize();
            String output = "mc\n"
                    + normalized.getLoc1() + ";" + normalized.getLoc2() + "\n"
                    + StringUtils.join(blocks, ";") + "\n"
                    + StringUtils.join(types, ";") + "\n"
                    + StringUtils.join(textures, ";");
            byte[] compress = compress(output);

            File file = new File(GlobalPlugin.i().getConfigDir() + "/" + path);
            if (!file.exists()) {
                file.mkdirs();
            }
            file.delete();
            file.createNewFile();

            FileUtils.writeByteArrayToFile(file, compress);
        } catch (IOException e) {
            GlobalPlugin.i().getLogger().log(Level.SEVERE, "Failed to save schematic", e);
        }
    }

    public void deleteFile() {
        File file = new File(GlobalPlugin.i().getConfigDir() + "/" + path);
        if (file.exists()) {
            file.delete();
        }
    }

    public void sendFakeOrLoad(Location origin, Set<? extends PlayerModel> players) {
        if (!loaded) {
            load(origin, false);
            loaded = true;
        }
        send(players);
    }

    public void send(Set<? extends PlayerModel> players) {
        for (PlayerModel data : players) {
            packets.forEach(data::sendPacket);
            tileEntityPackets.forEach(data::sendPacket);
        }
    }

    public void load(Location origin, boolean paste) {
        String regionString;
        String blocksString;
        String typesString;
        String texturesString;
        try (FileInputStream inputStream = new FileInputStream(GlobalPlugin.i().getConfigDir() + "/" + path)) {
            InputStreamReader reader = new InputStreamReader(new GZIPInputStream(inputStream), StandardCharsets.UTF_8);
            BufferedReader in = new BufferedReader(reader);

            if (!in.readLine().equals("mc")) {
                GlobalPlugin.i().getLogger().severe("Invalid format for " + path + ". Schematic was not loaded");
                return;
            }
            regionString = in.readLine();
            blocksString = in.readLine();
            typesString = in.readLine();
            texturesString = in.readLine();
        } catch (IOException e) {
            GlobalPlugin.i().getLogger().log(Level.SEVERE, "Failed to load schematic", e);
            return;
        }

        List<String> types = Arrays.asList(typesString.split(";"));
        List<String> textures = texturesString == null ? null : Arrays.asList(texturesString.split(";"));

        String[] regionSplit = regionString.split(";");
        Region region = new Region(regionSplit[0], regionSplit[1]);

        Map<SectionPos, Pair<List<Short>, List<BlockState>>> chunks = new HashMap<>();
        Queue<String> blocks = new ArrayDeque<>(Arrays.asList(blocksString.split(";")));
        region.forBlocks(origin, (x, y, z) -> {
            if (blocks.isEmpty()) return;

            String block = blocks.poll();
            int delimiterIndex = block.indexOf("[");
            if (delimiterIndex < 0) {
                block = types.get(Integer.parseInt(block));
            } else {
                block = types.get(Integer.parseInt(block.substring(0, delimiterIndex))) + block.substring(delimiterIndex);
            }
            block = fixBlockData(block);

            PlayerProfile textureProfile = null;
            if (textures != null) {
                String texture = null;
                int indexOfTexture = block.indexOf("?");
                if (indexOfTexture >= 0) {
                    texture = textures.get(Integer.parseInt(block.substring(indexOfTexture + 1)));
                    block = block.substring(0, indexOfTexture);
                }

                if (texture != null) {
                    CompoundTag compound = new CompoundTag();
                    compound.putString("id", "skull");
                    compound.putInt("x", x);
                    compound.putInt("y", y);
                    compound.putInt("z", z);

                    CompoundTag ownerTag = new CompoundTag();
                    textureProfile = Skins.createFakeProfile(texture);
                    serializePlayerProfile(ownerTag, textureProfile);
                    compound.put("SkullOwner", ownerTag);

                    SkullBlockEntity tileEntity = new SkullBlockEntity(new BlockPos(x, y, z), null);
                    tileEntityPackets.add(ClientboundBlockEntityDataPacket.create(tileEntity, t -> compound));
                }
            }

            BlockData blockData = Bukkit.createBlockData(block);
            BlockState state = ((CraftBlockData) blockData).getState();

            if (paste && origin.getWorld() != null) {
                Block craftBlock = origin.getWorld().getBlockAt(x, y, z);
                craftBlock.setBlockData(blockData, false);

                if (textureProfile != null) {
                    ((Skull) origin.getBlock().getState()).setPlayerProfile(textureProfile);
                }
            }

            int px = x & 0xF;
            int py = y & 0xF;
            int pz = z & 0xF;
            short position = (short) (px << 8 | pz << 4 | py);
            Pair<List<Short>, List<BlockState>> pair = chunks.computeIfAbsent(SectionPos.of(x >> 4, y >> 4, z >> 4), k -> Pair.of(new ArrayList<>(), new ArrayList<>()));
            pair.key().add(position);
            pair.value().add(state);
        });

        chunks.forEach((sectionPosition, pair) -> {
            short[] positions = new short[pair.key().size()];
            for (int i = 0; i < pair.key().size(); i++) {
                positions[i] = pair.key().get(i);
            }
            BlockState[] blocksData = pair.value().toArray(new BlockState[0]);

            LevelChunkSection chunkSection = new LevelChunkSection(null, null) {
                int index = 0;

                @Override
                public BlockState getBlockState(int i, int j, int k) {
                    return blocksData[index++];
                }

                @Override
                public void recalcBlockCounts() {
                }
            };
            ClientboundSectionBlocksUpdatePacket packet = new ClientboundSectionBlocksUpdatePacket(sectionPosition, new ShortArraySet(positions), chunkSection);
            packets.add(packet);
        });
    }

    private String fixBlockData(String blockData) {
        if (blockData.contains("grass_path")) {
            return blockData.replace("grass_path", "dirt_path");
        }

        if (blockData.contains("cauldron")) {
            if (blockData.equals("cauldron[level=0]")) {
                return "cauldron";
            }

            if (blockData.contains("level")) {
                return blockData.replace("cauldron", "water_cauldron");
            }
        }

        return blockData;
    }

    private Field getField(String name) throws Exception {
        Field field = ClientboundSectionBlocksUpdatePacket.class.getDeclaredField(name);
        field.setAccessible(true);

        return field;
    }

    private void serializePlayerProfile(CompoundTag nbtTagCompound, PlayerProfile playerProfile) {
        if (!StringUtils.isEmpty(playerProfile.getName())) {
            nbtTagCompound.putString("Name", playerProfile.getName());
        }

        if (playerProfile.getId() != null) {
            nbtTagCompound.putUUID("Id", playerProfile.getId());
        }

        if (!playerProfile.getProperties().isEmpty()) {
            CompoundTag properties = new CompoundTag();
            for (ProfileProperty property : playerProfile.getProperties()) {
                ListTag list = new ListTag();
                CompoundTag compound = new CompoundTag();
                compound.putString("Value", property.getValue());
                if (property.getSignature() != null) {
                    compound.putString("Signature", property.getSignature());
                }
                list.add(compound);
                properties.put(property.getName(), list);
            }

            nbtTagCompound.put("Properties", properties);
        }
    }

    public void loadAndPaste(Location origin) {
        load(origin, true);
    }
}
