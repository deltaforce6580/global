package net.mineclick.global.util;

import lombok.Getter;
import org.bukkit.ChatColor;

import java.util.function.Function;

public enum MessageType {
    NORMAL(s -> s),
    INFO(s -> ChatColor.GOLD + s),
    ERROR(s -> ChatColor.RED + s);

    @Getter
    private final Function<String, String> format;

    MessageType(Function<String, String> format) {
        this.format = format;
    }
}
