package net.mineclick.global.util.location;

import lombok.NonNull;
import lombok.Setter;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;

import java.text.DecimalFormat;

public class LocationParser {
    public static final DecimalFormat format = new DecimalFormat("###.##");
    @Setter
    private static World world = Bukkit.getWorlds().get(0);
    @Setter
    public static String regex = " ";

    public static Location parse(String args) {
        return parse(world, args);
    }

    public static Location parse(@NonNull World world, @NonNull String args) {
        Location location = new Location(world, 0, 0, 0);

        String[] split = args.split(regex);
        if (split.length >= 3) {
            location.setX(Double.parseDouble(split[0]));
            location.setY(Double.parseDouble(split[1]));
            location.setZ(Double.parseDouble(split[2]));

            if (split.length >= 4) {
                location.setYaw(Float.parseFloat(split[3]));
            }

            if (split.length >= 5) {
                location.setPitch(Float.parseFloat(split[4]));
            }
        }

        return location;
    }

    public static String toString(Location location) {
        String string = format.format(location.getX()) + regex + format.format(location.getY()) + regex + format.format(location.getZ());
        if (location.getYaw() != 0)
            string += regex + format.format(location.getYaw());
        if (location.getPitch() != 0)
            string += regex + format.format(location.getPitch());
        return string;
    }
}
