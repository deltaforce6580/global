package net.mineclick.global.util.location;

import net.mineclick.global.GlobalPlugin;
import org.bukkit.util.Vector;

public class RandomVector extends Vector {
    public RandomVector(boolean noY) {
        x = GlobalPlugin.random.nextDouble() - 0.5;
        if (!noY) {
            y = GlobalPlugin.random.nextDouble() - 0.5;
        }
        z = GlobalPlugin.random.nextDouble() - 0.5;

        normalize();
    }

    public RandomVector() {
        this(false);
    }

    public RandomVector(double magnitude) {
        this();
        multiply(magnitude);
    }
}
