package net.mineclick.global.util.location;

import lombok.Getter;
import lombok.Setter;
import net.mineclick.global.GlobalPlugin;
import net.mineclick.global.config.SaveConstructor;
import net.mineclick.global.model.PlayerModel;
import net.mineclick.global.service.PlayersService;
import net.mineclick.global.util.ParticlesUtil;
import net.mineclick.global.util.Runner;
import org.apache.logging.log4j.util.TriConsumer;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.configuration.ConfigurationSection;

import java.awt.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Getter
@SaveConstructor(args = {"loc1", "loc2"})
public class Region {
    private final Location location1;
    private final Location location2;

    private Location min;
    private Location max;
    private String loc1;
    private String loc2;
    private final ConfigLocation vLoc1;
    private final ConfigLocation vLoc2;
    @Setter
    private World world;
    private boolean visualizing;
    private Color visualColor = Color.GRAY;

    public Region(Location loc1, Location loc2) {
        this.location1 = loc1;
        this.location2 = loc2;

        findMinMax();
        world = loc1.getWorld();

        vLoc1 = new ConfigLocation(loc1);
        vLoc2 = new ConfigLocation(loc2);
    }

    public Region(Map<?, ?> map) {
        this(String.valueOf(map.get("loc1")), String.valueOf(map.get("loc2")));
    }

    public Region(ConfigurationSection section) {
        this(section.getString("loc1"), section.getString("loc2"));
    }

    public Region(String loc1, String loc2) {
        this(LocationParser.parse(loc1), LocationParser.parse(loc2));
    }

    public Region normalize() {
        int x = max.getBlockX() - min.getBlockX();
        int y = max.getBlockY() - min.getBlockY();
        int z = max.getBlockZ() - min.getBlockZ();

        return new Region("0 0 0", x + " " + y + " " + z);
    }

    @Override
    public String toString() {
        return "[{" + min.getBlockX() + " " + min.getBlockY() + " " + min.getBlockZ() + "}, {" + max.getBlockX() + " " + max.getBlockY() + " " + max.getBlockZ() + "}]";
    }

    private void findMinMax() {
        min = new Location(location1.getWorld(), Math.min(location1.getX(), location2.getX()), Math.min(location1.getY(), location2.getY()), Math.min(location1.getZ(), location2.getZ()));
        max = new Location(location1.getWorld(), Math.max(location1.getX(), location2.getX()), Math.max(location1.getY(), location2.getY()), Math.max(location1.getZ(), location2.getZ()));

        loc1 = LocationParser.toString(min);
        loc2 = LocationParser.toString(max);
    }

    public boolean isIn(Location location) {
        return isIn(location, false);
    }

    public boolean isIn(Location location, boolean ignoreY) {
        return location.getBlockX() <= max.getBlockX()
                && (ignoreY || location.getBlockY() <= max.getBlockY())
                && location.getBlockZ() <= max.getBlockZ()
                && location.getBlockX() >= min.getBlockX()
                && (ignoreY || location.getBlockY() >= min.getBlockY())
                && location.getBlockZ() >= min.getBlockZ();
    }

    public Location getRandomWithin() {
        int x = GlobalPlugin.random.nextInt(max.getBlockX() - min.getBlockX() + 1);
        int y = GlobalPlugin.random.nextInt(max.getBlockY() - min.getBlockY() + 1);
        int z = GlobalPlugin.random.nextInt(max.getBlockZ() - min.getBlockZ() + 1);

        return min.clone().add(x, y, z);
    }

    public List<Location> getLocationList() {
        List<Location> list = new ArrayList<>();
        for (int x = min.getBlockX(); x <= max.getBlockX(); x++) {
            for (int y = min.getBlockY(); y <= max.getBlockY(); y++) {
                for (int z = min.getBlockZ(); z <= max.getBlockZ(); z++) {
                    list.add(new Location(world, x, y, z));
                }
            }
        }

        return list;
    }

    public List<Block> getBlocks(Material... types) {
        List<Material> materials = Arrays.asList(types);
        return getLocationList().stream()
                .map(Location::getBlock)
                .filter(b -> materials.isEmpty() || materials.contains(b.getType()))
                .collect(Collectors.toList());
    }

    public void forBlocks(TriConsumer<Integer, Integer, Integer> consumer) {
        forBlocks(null, consumer);
    }

    public void forBlocks(Location offset, TriConsumer<Integer, Integer, Integer> consumer) {
        int offsetX = offset == null ? 0 : offset.getBlockX();
        int offsetY = offset == null ? 0 : offset.getBlockY();
        int offsetZ = offset == null ? 0 : offset.getBlockZ();

        for (int x = min.getBlockX() + offsetX; x <= max.getBlockX() + offsetX; x++) {
            for (int y = min.getBlockY() + offsetY; y <= max.getBlockY() + offsetY; y++) {
                for (int z = min.getBlockZ() + offsetZ; z <= max.getBlockZ() + offsetZ; z++) {
                    consumer.accept(x, y, z);
                }
            }
        }
    }

    public int getWidth() {
        return max.getBlockX() - min.getBlockX() + 1;
    }

    public int getHeight() {
        return max.getBlockY() - min.getBlockY() + 1;
    }

    public int getLength() {
        return max.getBlockZ() - min.getBlockZ() + 1;
    }

    public void visualize(World world, Color color) {
        visualColor = color;
        if (!visualizing) {
            visualizing = true;

            Runner.sync(0, 20, state -> {
                for (double x = 0; x <= getWidth(); x += 0.5) {
                    for (double y = 0; y <= getHeight(); y += 0.5) {
                        for (double z = 0; z <= getLength(); z += 0.5) {
                            int onEdge = (x == 0 || x == getWidth()) ? 1 : 0;
                            onEdge += (y == 0 || y == getHeight()) ? 1 : 0;
                            onEdge += (z == 0 || z == getLength()) ? 1 : 0;

                            if (onEdge > 1) {
                                Location b = new Location(world, min.getBlockX() + x, min.getBlockY() + y, min.getBlockZ() + z);
                                ParticlesUtil.sendColor(b, visualColor, PlayersService.i().getAll());
                            }
                        }
                    }
                }

                if (!visualizing) {
                    state.cancel();
                }
            });
        }
    }

    public void visualizeEnds(PlayerModel player) {
        vLoc1.visualize(player);
        vLoc2.visualize(player);
    }

    public void stopVisualizing() {
        visualizing = false;
    }

    public void stopVisualizingEnds(PlayerModel player) {
        vLoc1.stopVisualizing(player);
        vLoc2.stopVisualizing(player);
    }

    public void resetVisualize() {
        visualColor = Color.GRAY;
    }
}
