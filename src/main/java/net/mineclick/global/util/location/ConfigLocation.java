package net.mineclick.global.util.location;

import lombok.AccessLevel;
import lombok.Getter;
import net.mineclick.global.config.SaveConstructor;
import net.mineclick.global.model.PlayerModel;
import net.mineclick.global.util.ParticlesUtil;
import net.mineclick.global.util.Runner;
import org.bukkit.Location;
import org.bukkit.World;

import java.awt.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Getter
@SaveConstructor(args = "loc")
public class ConfigLocation {
    @Getter(AccessLevel.NONE)
    private final Set<PlayerModel> visualizingSet = new HashSet<>();
    @Getter(AccessLevel.NONE)
    private List<Location> visualizingBlocks;
    private final String loc;

    public ConfigLocation(String loc) {
        this.loc = loc;
    }

    public ConfigLocation(Location loc) {
        this.loc = LocationParser.toString(loc);
    }

    public Location toLocation() {
        return LocationParser.parse(loc);
    }

    public void visualize(PlayerModel player) {
        visualize(player, Color.RED);
    }

    public void visualize(PlayerModel player, Color color) {
        if (player.isOffline()) return;
        Location l = toLocation();

        if (visualizingBlocks == null) {
            World world = player.getPlayer().getWorld();
            visualizingBlocks = new ArrayList<>();

            for (double x = 0; x <= 1; x += 0.5) {
                for (double y = 0; y <= 1; y += 0.5) {
                    for (double z = 0; z <= 1; z += 0.5) {
                        int onEdge = (x == 0 || x == 1) ? 1 : 0;
                        onEdge += (y == 0 || y == 1) ? 1 : 0;
                        onEdge += (z == 0 || z == 1) ? 1 : 0;

                        if (onEdge > 1) {
                            visualizingBlocks.add(new Location(world, l.getBlockX() + x, l.getBlockY() + y, l.getBlockZ() + z));
                        }
                    }
                }
            }
        }

        visualizingSet.add(player);
        Runner.sync(0, 10, state -> {
            if (!visualizingSet.contains(player) || !player.isOnline()) {
                visualizingSet.remove(player);
                state.cancel();
                return;
            }

            Location location = player.getPlayer().getLocation();
            visualizingBlocks
                    .stream().filter(b -> b.distanceSquared(location) <= 400)
                    .forEach(b -> ParticlesUtil.sendColor(b, color, player));
        });
    }

    public void stopVisualizing(PlayerModel player) {
        visualizingSet.remove(player);
    }

    @Override
    public String toString() {
        return loc;
    }
}
