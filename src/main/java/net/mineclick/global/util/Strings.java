package net.mineclick.global.util;

import com.google.common.collect.ImmutableList;
import net.md_5.bungee.api.chat.*;
import net.mineclick.global.GlobalPlugin;
import net.mineclick.global.commands.Commands;
import net.mineclick.global.model.ChatSenderData;
import net.mineclick.global.model.PlayerModel;
import org.apache.commons.lang.StringUtils;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import java.util.List;

public class Strings {
    private final static int CENTER_PX = 154;
    private final static List<String> funnyReasons = ImmutableList.of(
            "space-time ruptured",
            "Herobrine appeared",
            "minions are the ones to blame",
            "we are on strike",
            "we are lazy",
            "something broke",
            "we didn't like it",
            "whatever",
            "who cares",
            "because",
            "you broke it",
            "Windows was updating",
            "packets got lost",
            "we spilled coffee on keyboard",
            "we don't get paid for this",
            "mom says no",
            "we are on vacation",
            "java.lang.NullPointerException: null",
            "1+1=2",
            "monkeys attacked us",
            "laziness gets the best of us",
            "loading took too long",
            "we tried to turn it off and back on again",
            "pizza is here",
            "we ran out of coffee",
            "blah-blah",
            "you didn't ask nicely",
            "we got other things to do",
            "Never gonna give you up. Never gonna let you down...",
            "someone let the dogs out",
            "that backflip didn't go well",
            "we tried to do parkour",
            "we can't type this fast",
            "danger to manifold",
            "you asked too many times",
            "null",
            "<random_excuse>",
            "we like trains",
            "our lawyer says we can't do that",
            "meow",
            ":P"
    );

    public static String line() {
        return ChatColor.GRAY + ChatColor.STRIKETHROUGH.toString() + ChatColor.BOLD + StringUtils.repeat("-", 45) + ChatColor.RESET;
    }

    public static TextComponent middle(TextComponent message) {
        String middle = middle(message.toLegacyText());
        char[] charArray = middle.toCharArray();
        int spaceCount = 0;
        for (int i = 0; i < charArray.length; i++) {
            if (charArray[i] != ' ') {
                spaceCount = i + 1;
                break;
            }
        }

        TextComponent textComponent = new TextComponent(StringUtils.repeat(" ", spaceCount));
        textComponent.addExtra(message);
        return textComponent;
    }

    public static String middle(String message) {
        if (message == null || message.equals(""))
            return message;

        message = ChatColor.translateAlternateColorCodes('&', message);

        int messagePxSize = 0;
        boolean previousCode = false;
        boolean isBold = false;

        for (char c : message.toCharArray()) {
            if (c == '§') {
                previousCode = true;
            } else if (previousCode) {
                previousCode = false;
                isBold = c == 'l' || c == 'L';
            } else {
                DefaultFontInfo dFI = DefaultFontInfo.getDefaultFontInfo(c);
                messagePxSize += isBold ? dFI.getBoldLength() : dFI.getLength();
                messagePxSize++;
            }
        }

        int halvedMessageSize = messagePxSize / 2;
        int toCompensate = CENTER_PX - halvedMessageSize;
        int spaceLength = DefaultFontInfo.SPACE.getLength() + 1;
        int compensated = 0;
        StringBuilder sb = new StringBuilder();
        while (compensated < toCompensate) {
            sb.append(" ");
            compensated += spaceLength;
        }

        return sb + message;
    }

    public static String getFunnyReason() {
        return funnyReasons.get(GlobalPlugin.random.nextInt(funnyReasons.size()));
    }

    public static void sendWebsite(Player player) {
        sendWebsite(player, ChatColor.YELLOW + "Website -> " + ChatColor.DARK_AQUA + "mineclick.net " + ChatColor.GRAY + "[click]");
    }

    public static void sendWebsite(Player player, String msg) {
        TextComponent text = new TextComponent(msg);
        text.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("Click to visit our website").color(net.md_5.bungee.api.ChatColor.DARK_AQUA).create()));
        text.setClickEvent(new ClickEvent(ClickEvent.Action.OPEN_URL, "http://mineclick.net/"));
        player.spigot().sendMessage(text);
    }

    public static void sendStore(Player player) {
        sendStore(player, ChatColor.YELLOW + "Store -> " + ChatColor.DARK_AQUA + "store.mineclick.net " + ChatColor.GRAY + "[click]");
    }

    public static void sendWiki(Player player) {
        String msg = ChatColor.YELLOW + "Wiki -> " + ChatColor.DARK_AQUA + "wiki.mineclick.net " + ChatColor.GRAY + "[click]";

        TextComponent text = new TextComponent(msg);
        text.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("Click to visit our wiki").color(net.md_5.bungee.api.ChatColor.DARK_AQUA).create()));
        text.setClickEvent(new ClickEvent(ClickEvent.Action.OPEN_URL, "https://wiki.mineclick.net/"));
        player.spigot().sendMessage(text);
    }

    public static void sendStore(Player player, String msg) {
        TextComponent text = new TextComponent(msg);
        text.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("Click to visit our store").color(net.md_5.bungee.api.ChatColor.DARK_AQUA).create()));
        text.setClickEvent(new ClickEvent(ClickEvent.Action.OPEN_URL, "https://store.mineclick.net/"));
        player.spigot().sendMessage(text);
    }

    public static void sendRules(Player player) {
        sendRules(player, ChatColor.YELLOW + "Rules -> " + ChatColor.DARK_AQUA + "https://wiki.mineclick.net/en/rules " + ChatColor.GRAY + "[click]");
    }

    public static void sendRules(Player player, String msg) {
        TextComponent text = new TextComponent(msg);
        text.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("Click to visit our rules page").color(net.md_5.bungee.api.ChatColor.DARK_AQUA).create()));
        text.setClickEvent(new ClickEvent(ClickEvent.Action.OPEN_URL, "https://wiki.mineclick.net/en/rules"));
        player.spigot().sendMessage(text);
    }

    public static void sendDiscord(Player player) {
        sendDiscord(player, ChatColor.YELLOW + "Discord -> " + ChatColor.DARK_AQUA + "https://discord.gg/AXhUE5G " + ChatColor.GRAY + "[click]");
    }

    public static void sendDiscord(Player player, String msg) {
        TextComponent text = new TextComponent(msg);
        text.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("Click to visit our Discord server").color(net.md_5.bungee.api.ChatColor.DARK_AQUA).create()));
        text.setClickEvent(new ClickEvent(ClickEvent.Action.OPEN_URL, "https://discord.gg/AXhUE5G"));
        player.spigot().sendMessage(text);
    }

    public static TextComponent createButtons(PlayerModel playerModel, String left, String right, Runnable onLeftClick, Runnable onRightClick) {
        int spaces = 0;
        for (char c : Strings.middle(left.toUpperCase() + "    " + right.toUpperCase()).toCharArray()) {
            if (c == ' ') {
                spaces++;
            } else {
                break;
            }
        }

        TextComponent text = new TextComponent(StringUtils.repeat(" ", spaces));

        BaseComponent leftComp = new ComponentBuilder(left.toUpperCase()).color(net.md_5.bungee.api.ChatColor.GREEN).create()[0];
        leftComp.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("Click to " + left).color(net.md_5.bungee.api.ChatColor.GREEN).create()));
        leftComp.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/" + Commands.makeSecretCommand((playerData, strings) -> {
            if (!playerData.equals(playerModel)) {
                return false;
            }
            onLeftClick.run();
            return true;
        })));

        BaseComponent rightComponent = new ComponentBuilder(right.toUpperCase()).color(net.md_5.bungee.api.ChatColor.RED).create()[0];
        rightComponent.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("Click to " + right).color(net.md_5.bungee.api.ChatColor.RED).create()));
        rightComponent.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/" + Commands.makeSecretCommand((playerData, strings) -> {
            if (playerData != playerModel) {
                return false;
            }
            onRightClick.run();
            return true;
        })));

        text.addExtra(leftComp);
        text.addExtra("    ");
        text.addExtra(rightComponent);

        return text;
    }

    public static TextComponent playerHoverInfo(ChatSenderData data) {
        return playerHoverInfo(data, "Click to see more", "/playerinfo", true);
    }

    public static TextComponent playerHoverInfo(ChatSenderData data, String hoverActionMsg, String command, boolean run) {
        StringBuilder hoverBuilder = new StringBuilder();
        hoverBuilder.append(ChatColor.GOLD).append(data.getName()).append("'s info").append("\n")
                .append("\n").append(ChatColor.GRAY).append("Rank: ")
                .append(data.getRank().getPrefix()).append(data.getRank().getName());
        if (data.getInfo() != null) {
            data.getInfo().forEach((label, value) -> {
                hoverBuilder.append("\n").append(ChatColor.GRAY).append(label).append(": ").append(ChatColor.YELLOW).append(value);
            });
        }
        hoverBuilder.append("\n").append("\n").append(ChatColor.DARK_GRAY).append(hoverActionMsg);

        return new TextComponent(new ComponentBuilder(data.getRank().getChatPrefix().concat(data.getName()))
                .event(new HoverEvent(HoverEvent.Action.SHOW_TEXT, TextComponent.fromLegacyText(hoverBuilder.toString())))
                .event(new ClickEvent(run ? ClickEvent.Action.RUN_COMMAND : ClickEvent.Action.SUGGEST_COMMAND, command + " " + data.getName() + " "))
                .create());
    }

    public enum DefaultFontInfo {
        A('A', 5),
        a('a', 5),
        B('B', 5),
        b('b', 5),
        C('C', 5),
        c('c', 5),
        D('D', 5),
        d('d', 5),
        E('E', 5),
        e('e', 5),
        F('F', 5),
        f('f', 4),
        G('G', 5),
        g('g', 5),
        H('H', 5),
        h('h', 5),
        I('I', 3),
        i('i', 1),
        J('J', 5),
        j('j', 5),
        K('K', 5),
        k('k', 4),
        L('L', 5),
        l('l', 1),
        M('M', 5),
        m('m', 5),
        N('N', 5),
        n('n', 5),
        O('O', 5),
        o('o', 5),
        P('P', 5),
        p('p', 5),
        Q('Q', 5),
        q('q', 5),
        R('R', 5),
        r('r', 5),
        S('S', 5),
        s('s', 5),
        T('T', 5),
        t('t', 4),
        U('U', 5),
        u('u', 5),
        V('V', 5),
        v('v', 5),
        W('W', 5),
        w('w', 5),
        X('X', 5),
        x('x', 5),
        Y('Y', 5),
        y('y', 5),
        Z('Z', 5),
        z('z', 5),
        NUM_1('1', 5),
        NUM_2('2', 5),
        NUM_3('3', 5),
        NUM_4('4', 5),
        NUM_5('5', 5),
        NUM_6('6', 5),
        NUM_7('7', 5),
        NUM_8('8', 5),
        NUM_9('9', 5),
        NUM_0('0', 5),
        EXCLAMATION_POINT('!', 1),
        AT_SYMBOL('@', 6),
        NUM_SIGN('#', 5),
        DOLLAR_SIGN('$', 5),
        PERCENT('%', 5),
        UP_ARROW('^', 5),
        AMPERSAND('&', 5),
        ASTERISK('*', 5),
        LEFT_PARENTHESIS('(', 4),
        RIGHT_PERENTHESIS(')', 4),
        MINUS('-', 5),
        UNDERSCORE('_', 5),
        PLUS_SIGN('+', 5),
        EQUALS_SIGN('=', 5),
        LEFT_CURL_BRACE('{', 4),
        RIGHT_CURL_BRACE('}', 4),
        LEFT_BRACKET('[', 3),
        RIGHT_BRACKET(']', 3),
        COLON(':', 1),
        SEMI_COLON(';', 1),
        DOUBLE_QUOTE('"', 3),
        SINGLE_QUOTE('\'', 1),
        LEFT_ARROW('<', 4),
        RIGHT_ARROW('>', 4),
        QUESTION_MARK('?', 5),
        SLASH('/', 5),
        BACK_SLASH('\\', 5),
        LINE('|', 1),
        TILDE('~', 5),
        TICK('`', 2),
        PERIOD('.', 1),
        COMMA(',', 1),
        SPACE(' ', 3),
        DEFAULT('a', 4);

        private final char character;
        private final int length;

        DefaultFontInfo(char character, int length) {
            this.character = character;
            this.length = length;
        }

        public static DefaultFontInfo getDefaultFontInfo(char c) {
            for (DefaultFontInfo dFI : DefaultFontInfo.values()) {
                if (dFI.getCharacter() == c) return dFI;
            }
            return DefaultFontInfo.DEFAULT;
        }

        public char getCharacter() {
            return this.character;
        }

        public int getLength() {
            return this.length;
        }

        public int getBoldLength() {
            if (this == DefaultFontInfo.SPACE) return this.getLength();
            return this.length + 1;
        }
    }
}
