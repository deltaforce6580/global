package net.mineclick.global.util;

import io.github.classgraph.ClassGraph;
import io.github.classgraph.ScanResult;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class SingletonInitializer {
    public static void loadAll(ClassLoader classLoader) {
        try (ScanResult scan = new ClassGraph().enableAllInfo().acceptPackages("net.mineclick").addClassLoader(classLoader).scan()) {
            for (Class<?> clazz : scan.getClassesWithAnnotation(SingletonInit.class.getName()).loadClasses()) {
                SingletonInit annotation = clazz.getAnnotation(SingletonInit.class);
                if (annotation != null) {
                    String methodName = annotation.instanceMethod();
                    try {
                        Method declaredMethod = clazz.getDeclaredMethod(methodName);
                        declaredMethod.setAccessible(true);
                        declaredMethod.invoke(null);
                    } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }
}
