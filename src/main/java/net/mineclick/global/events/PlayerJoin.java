package net.mineclick.global.events;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import net.mineclick.global.model.PlayerModel;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

@Getter
@RequiredArgsConstructor
public class PlayerJoin extends Event {
    private static final HandlerList handlers = new HandlerList();

    private final PlayerModel player;
    private final boolean networkWide;

    public static HandlerList getHandlerList() {
        return handlers;
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }
}
