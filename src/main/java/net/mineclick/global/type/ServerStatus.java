package net.mineclick.global.type;

public enum ServerStatus {
    STARTING,
    RUNNING,
    WAITING_TO_RESTART,
    RESTARTING;

    public boolean isRunning() {
        return this.equals(RUNNING) || this.equals(WAITING_TO_RESTART);
    }
}
