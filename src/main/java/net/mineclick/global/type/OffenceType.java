package net.mineclick.global.type;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import net.mineclick.global.util.Pair;

import java.util.function.Function;

@Getter
@RequiredArgsConstructor
public enum OffenceType {
    FLYING("flying", count -> {
        // First offence: kick
        if (count <= 1) {
            return Pair.of(PunishmentType.KICK, 0);
        }

        // Otherwise: ban for good
        return Pair.of(PunishmentType.BAN, -1);
    }),
    AUTOCLICKING("autoclicking", count -> {
        // First offence: warn
        if (count <= 1) {
            return Pair.of(PunishmentType.WARNING, 0);
        }

        // Second: kick
        if (count <= 2) {
            return Pair.of(PunishmentType.KICK, 0);
        }

        // Third to fifth: ban for 30, 120, 270 min
        if (count <= 5) {
            return Pair.of(PunishmentType.BAN, 30 * ((count - 2) * (count - 2)));
        }

        // Sixth and seventh: ban for 1, 2 days
        if (count <= 7) {
            return Pair.of(PunishmentType.BAN, 60 * 24 * (count - 5));
        }

        // Otherwise: ban for good
        return Pair.of(PunishmentType.BAN, -1);
    }),
    SWEARING("swearing", count -> {
        // First: mute for 15 min
        if (count <= 1) {
            return Pair.of(PunishmentType.MUTE, 15);
        }

        // Second: mute for a day
        if (count <= 2) {
            return Pair.of(PunishmentType.MUTE, 60 * 24);
        }

        // Third: ban for a day
        if (count <= 3) {
            return Pair.of(PunishmentType.BAN, 60 * 24);
        }

        // Otherwise: ban for good
        return Pair.of(PunishmentType.BAN, -1);
    }),
    HARASSING("harassing/bullying", count -> {
        // First: mute for 60 min
        if (count <= 1) {
            return Pair.of(PunishmentType.MUTE, 60);
        }

        // Second: ban for a day
        if (count <= 2) {
            return Pair.of(PunishmentType.BAN, 60 * 24);
        }

        // Otherwise: ban for good
        return Pair.of(PunishmentType.BAN, -1);
    }),
    AVOIDING_AFK("avoiding afk detection", count -> {
        // First: kick
        if (count <= 1) {
            return Pair.of(PunishmentType.KICK, 0);
        }

        // Second: ban for a day
        if (count <= 2) {
            return Pair.of(PunishmentType.BAN, 60 * 24);
        }

        // Otherwise: ban for good
        return Pair.of(PunishmentType.BAN, -1);
    }),
    SPAMMING("spamming/advertising", count -> {
        // First: mute for 60 min
        if (count <= 1) {
            return Pair.of(PunishmentType.MUTE, 60);
        }

        // Second: ban for a day
        if (count <= 2) {
            return Pair.of(PunishmentType.BAN, 60 * 24);
        }

        // Otherwise: ban for good
        return Pair.of(PunishmentType.BAN, -1);
    }),
    IMPERSONATING_STAFF("impersonating staff", count -> {
        // First: warn
        if (count <= 1) {
            return Pair.of(PunishmentType.WARNING, 0);
        }

        // Second: mute for 30 min
        if (count <= 2) {
            return Pair.of(PunishmentType.MUTE, 30);
        }

        // Otherwise: ban for a day
        return Pair.of(PunishmentType.BAN, 60 * 24);
    }),
    SKIN("inappropriate skin and/or username", count -> {
        // First: kick
        if (count <= 1) {
            return Pair.of(PunishmentType.KICK, 0);
        }

        // Second: ban for good
        return Pair.of(PunishmentType.BAN, -1);
    }),
    OTHER("being a baddie", count -> {
        // First: warn
        if (count <= 1) {
            return Pair.of(PunishmentType.WARNING, 0);
        }

        // Second: kick
        if (count <= 2) {
            return Pair.of(PunishmentType.KICK, 0);
        }

        // Otherwise: ban for a day
        return Pair.of(PunishmentType.BAN, 60 * 24);
    }),
    ;

    private final String reason;
    private final Function<Integer, Pair<PunishmentType, Integer>> suggestion;
}
