package net.mineclick.global.type;

import lombok.Getter;
import org.bukkit.ChatColor;

@Getter
public enum Rank {
    DEFAULT(0, "Default", ChatColor.GRAY.toString(), ChatColor.BLUE.toString()),
    PAID(1, "Premium", ChatColor.DARK_PURPLE + "[❤] " + ChatColor.DARK_AQUA),
    YOUTUBER(2, "YouTuber", ChatColor.translateAlternateColorCodes('&', "&4[&f&l▶&4] &6")),
    TWITCH(2, "Twitch Streamer", ChatColor.translateAlternateColorCodes('&', "&5[&f▏▏&5] &d")),
    STAFF(8, "Staff", ChatColor.translateAlternateColorCodes('&', "&2&l[Ⓢ] &a")),
    SUPER_STAFF(9, "Admin", ChatColor.translateAlternateColorCodes('&', "&2&l[Ⓢ] &a")),
    DEV(10, "Developer", ChatColor.translateAlternateColorCodes('&', "&2&l[&2☕&l] &a"));

    private final int weight;
    private final String name;
    private final String prefix;
    private final String chatPrefix;

    Rank(int weight, String name, String prefix) {
        this(weight, name, prefix, prefix);
    }

    Rank(int weight, String name, String prefix, String chatPrefix) {
        this.weight = weight;
        this.name = name;
        this.prefix = prefix;
        this.chatPrefix = chatPrefix;
    }

    public static Rank get(String name) {
        for (Rank rank : values()) {
            if (rank.toString().equalsIgnoreCase(name))
                return rank;
        }

        return DEFAULT;
    }

    public boolean isAtLeast(Rank rank) {
        return weight >= rank.weight;
    }
}
