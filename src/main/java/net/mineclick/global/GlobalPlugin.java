package net.mineclick.global;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import lombok.Getter;
import net.mineclick.core.messenger.InstantConverter;
import net.mineclick.core.messenger.Messenger;
import net.mineclick.core.messenger.redisWrapper.JedisWrapper;
import net.mineclick.global.commands.StaffCommands;
import net.mineclick.global.commands.UserCommands;
import net.mineclick.global.config.DimensionConfig;
import net.mineclick.global.config.MineshaftConfig;
import net.mineclick.global.service.ExceptionService;
import net.mineclick.global.service.ServersService;
import net.mineclick.global.type.ServerStatus;
import net.mineclick.global.util.Runner;
import net.mineclick.global.util.SingletonInitializer;
import net.mineclick.global.util.ui.InventoryListener;
import org.bukkit.Bukkit;
import org.bukkit.GameRule;
import org.bukkit.World;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.craftbukkit.v1_20_R1.CraftServer;
import org.bukkit.generator.ChunkGenerator;
import org.bukkit.plugin.java.JavaPlugin;
import org.jetbrains.annotations.NotNull;

import java.lang.reflect.Field;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Objects;
import java.util.Random;

/**
 * Author: MineClick
 * Date: 8/11/2017
 */
@Getter
public class GlobalPlugin extends JavaPlugin {
    public static final Random random = new Random();
    private static GlobalPlugin i;

    private final Gson gson = new GsonBuilder()
            .registerTypeAdapter(InstantConverter.TYPE, new InstantConverter())
            .registerTypeAdapterFactory(new GsonFactory())
            .create();
    private String configDir;
    private String serverId;

    public GlobalPlugin() {
        i = this;
    }

    public static GlobalPlugin i() {
        return i;
    }

    @Override
    public void onEnable() {
        if (Constants.PROD) {
            try {
                serverId = InetAddress.getLocalHost().getHostName();
                getLogger().info("HOSTNAME: " + serverId);
            } catch (UnknownHostException e) {
                getLogger().severe("CANNOT RESOLVE SERVER HOSTNAME! GAME WILL NOT START!");
                e.printStackTrace();
                return;
            }
        } else {
            serverId = "localhost";
        }

        // Jar config file
        saveDefaultConfig();

        // System config directory path
        configDir = System.getProperty("user.dir") + "/config";
        getLogger().info("Config directory set to: " + configDir);

        // Default generator and world rules
        CraftServer craftServer = (CraftServer) Bukkit.getServer();
        try {
            Field configField = CraftServer.class.getDeclaredField("configuration");
            configField.setAccessible(true);
            YamlConfiguration config = (YamlConfiguration) configField.get(craftServer);

            config.createSection("worlds").createSection("world").set("generator", "Global");
            Objects.requireNonNull(config.getConfigurationSection("worlds")).createSection("default").set("generator", "Global");

            for (World world : Bukkit.getWorlds()) {
                world.setGameRule(GameRule.ANNOUNCE_ADVANCEMENTS, false);
            }

            getLogger().info("Set default generator!");
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }

        new Messenger(
                serverId,
                new JedisWrapper(Constants.REDIS_HOST, Constants.REDIS_PORT, Constants.REDIS_PASSWORD),
                getLogger(),
                gson
        );

        SingletonInitializer.loadAll(this.getClassLoader());

        StaffCommands.load();
        UserCommands.load();
        new InventoryListener();

        // Custom Exceptions handler
        Bukkit.getLogger().addHandler(ExceptionService.i());
        Runtime.getRuntime().addShutdownHook(ServersService.i().createShutdownHook());

        Runner.sync(() -> {
            DimensionConfig.loadFiles();
            MineshaftConfig.loadFromFile();
            ServersService.i().setStatus(ServerStatus.RUNNING);
        });
    }

    @Override
    public void onDisable() {
        try {
            // Prevent spigot from shutting down before the other shutdown hook is finished
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public ChunkGenerator getDefaultWorldGenerator(@NotNull String worldName, String id) {
        return ServersService.i().getDefaultWorldGenerator();
    }
}