package net.mineclick.global.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.*;

@Getter
@Setter
@NoArgsConstructor
public class FriendsData {
    public static final int MAX_FRIENDS = 50;

    private List<UUID> friends = new ArrayList<>();
    private List<UUID> sentRequests = new ArrayList<>();
    private List<UUID> receivedRequests = new ArrayList<>();
    private Map<String, Long> requestsCooldown = new HashMap<>();

    public boolean isFriendsWith(UUID player) {
        return friends.contains(player);
    }

    public boolean hasSentRequest(UUID to) {
        return sentRequests.contains(to);
    }

    public boolean hasReceivedRequest(UUID from) {
        return receivedRequests.contains(from);
    }
}
