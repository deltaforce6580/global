package net.mineclick.global.model;

import lombok.Data;

@Data
public class ConfigurationsModel {
    private String configurations;
}
