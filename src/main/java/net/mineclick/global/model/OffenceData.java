package net.mineclick.global.model;

import lombok.Data;
import net.mineclick.global.type.OffenceType;
import net.mineclick.global.type.PunishmentType;

import java.time.Instant;

@Data
public class OffenceData {
    private OffenceType type;
    private PunishmentType punishment;
    private int durationMinutes;
    private String punishedBy;
    private Instant punishedOn;
    private String pardonedBy;
    private Instant pardonedOn;
    private String deniedBy;
    private Instant deniedOn;
    private Instant appealedOn;
    private String appealMessage;

    public boolean isPermanent() {
        return durationMinutes < 0;
    }

    public boolean isStillPunished() {
        return pardonedOn == null
                && (durationMinutes < 0 || Instant.now().isBefore(punishedOn.plusSeconds(durationMinutes * 60L)));
    }
}
