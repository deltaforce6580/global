package net.mineclick.global.model;

import lombok.Data;
import org.bukkit.Location;

@Data
public class OnJoinData {
    private String tpToPlayer;
    private String visitPlayer;
    private boolean tpToLobby;
    private String msg;
    private Location lastLocation;
}
