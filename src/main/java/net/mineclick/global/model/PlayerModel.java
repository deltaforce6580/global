package net.mineclick.global.model;

import com.destroystokyo.paper.profile.PlayerProfile;
import com.destroystokyo.paper.profile.ProfileProperty;
import com.google.common.collect.Lists;
import lombok.*;
import net.md_5.bungee.api.chat.TextComponent;
import net.mineclick.global.GlobalPlugin;
import net.mineclick.global.service.FriendsService;
import net.mineclick.global.service.OffencesService;
import net.mineclick.global.service.PlayersService;
import net.mineclick.global.service.WhitelistService;
import net.mineclick.global.type.PunishmentType;
import net.mineclick.global.type.Rank;
import net.mineclick.global.util.MessageType;
import net.mineclick.global.util.MessageUtil;
import net.mineclick.global.util.Runner;
import net.mineclick.global.util.Strings;
import net.minecraft.network.protocol.Packet;
import net.minecraft.server.network.ServerPlayerConnection;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.craftbukkit.v1_20_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;

import java.time.Instant;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;

@Getter
@Setter
@NoArgsConstructor
public class PlayerModel extends PlayerId {
    private Map<String, Instant> ips = new HashMap<>();
    private ChatData chatData = new ChatData();
    private List<OffenceData> offences = Lists.newArrayList();
    private GameSettings gameSettings = new GameSettings();
    private FriendsData friendsData = new FriendsData();
    private OnJoinData onJoinData = new OnJoinData();
    private String discordState; // a discord linking secret string

    private transient boolean destroyed;
    private transient String statusMsg;
    private transient Map<Runnable, AtomicInteger> scheduledTasks = new HashMap<>();
    private transient int errorCount = 0;
    private transient StackTraceElement lastExceptionTrace = null;
    @Setter(AccessLevel.NONE)
    private transient long ticks = 0;
    private transient long lastCommandExecutedAt = 0;
    private transient Player cachedBukkitPlayer;

    @SneakyThrows
    public static <T extends PlayerModel> T createNew(UUID uuid, Class<T> tClass) {
        T playerModel = tClass.getDeclaredConstructor().newInstance();
        playerModel.setUuid(uuid);
        return playerModel;
    }

    public boolean load() {
        Player player = getPlayer();
        if (player != null) {
            setName(player.getName());
            ips.put(player.getAddress().getHostString().replace('.', '-'), Instant.now());

            // Check if whitelisted
            if (WhitelistService.i().isEnabled() && !isRankAtLeast(Rank.STAFF) && !WhitelistService.i().contains(getName())) {
                Runner.sync(() -> player.kickPlayer(ChatColor.RED + "Unauthorized\n\n" + ChatColor.GRAY + "Contact staff if you believe this is an error"));
                return false;
            }

            // Check if the player is banned
            if (OffencesService.i().isCurrentlyPunishedWith(this, PunishmentType.BAN)) {
                setBanned(true); // keeping this for Store use

                Runner.sync(() -> PunishmentType.BAN.getApply().accept(this, OffencesService.i().getHighestOffence(this, PunishmentType.BAN)));
                return false;
            }
            setBanned(false);

            //Save texture
            setPing(player.getPing());
            PlayerProfile profile = player.getPlayerProfile();
            for (ProfileProperty property : profile.getProperties()) {
                if (property.getName().equals("textures")) {
                    setTexture(property.getValue());
                    setSignature(property.getSignature());
                    break;
                }
            }

            // Finally, call on load
            try {
                onLoad();
            } catch (Exception e) {
                GlobalPlugin.i().getLogger().log(Level.SEVERE, "Error loading the player record", e);
            }

            //Ticking runner
            Runner.sync(0, 1, (state) -> {
                if (isOffline()) {
                    state.cancel();
                } else {
                    String errorHash = "";
                    boolean errored = false;
                    try {
                        ticks = state.getTicks();
                        tick(ticks);
                        internalTick(ticks);
                    } catch (Exception e) {
                        errored = true;
                        errorCount++;

                        StackTraceElement element = e.getStackTrace().length > 0 ? e.getStackTrace()[0] : null;
                        errorHash = Integer.toString(Math.abs(element != null ? element.hashCode() : e.hashCode()), 36).toUpperCase();
                        if (!Objects.equals(element, lastExceptionTrace)) {
                            lastExceptionTrace = element;

                            GlobalPlugin.i().getLogger().log(Level.SEVERE, "[" + errorHash + "] Error on player tick (" + getName() + " " + getUuid() + ")" +
                                    "\nConsecutive errors will not be logged and after 5 seconds of continuous errors the player will be kicked", e);
                        }
                    }

                    // kick a player after erroring for 5 seconds straight
                    if (errored) {
                        if (errorCount >= 100) {
                            setDestroyed(true);

                            getPlayer().kickPlayer(ChatColor.RED + "Unexpected error occurred " + errorHash + "\n\n" + ChatColor.GRAY + "Please rejoin or contact staff with the error code if this happens again");
                        }
                    } else {
                        errorCount = 0;
                    }
                }
            });
        }

        return true;
    }

    public Player getPlayer() {
        return cachedBukkitPlayer != null ? cachedBukkitPlayer : (cachedBukkitPlayer = Bukkit.getPlayer(getUuid()));
    }

    public boolean isOffline() {
        return destroyed || (getPlayer() == null && (destroyed = true));
    }

    public boolean isRankAtLeast(Rank rank) {
        return getRank() != null && getRank().isAtLeast(rank);
    }

    public void sendPacket(Packet<?>... packets) {
        if (isOffline())
            return;

        ServerPlayerConnection connection = ((CraftPlayer) getPlayer()).getHandle().connection;
        for (Packet<?> packet : packets) {
            connection.send(packet);
        }
    }

    // Sending messages
    public void sendMessage(String message) {
        sendMessage(message, MessageType.NORMAL);
    }

    public void sendMessage(String message, MessageType type) {
        Player player = getPlayer();
        if (player != null) {
            player.sendMessage(type.getFormat().apply(message));
        }
    }

    public void sendMessage(TextComponent message) {
        Player player = getPlayer();
        if (player != null) {
            player.spigot().sendMessage(message);
        }
    }

    public void sendImportantMessage(String message, String secondMessage) {
        Player player = getPlayer();
        if (player == null) return;

        StringBuilder builder = new StringBuilder();
        builder.append(Strings.line()).append("\n");
        builder.append(Strings.middle(ChatColor.YELLOW + message));

        if (secondMessage != null) {
            builder.append("\n ").append("\n").append(Strings.middle(ChatColor.GRAY + secondMessage));
        }
        builder.append("\n").append(Strings.line());

        player.sendMessage(builder.toString());
    }

    private void internalTick(long ticks) {
        // Clear bukkit player cache
        cachedBukkitPlayer = null;

        // update ping every second
        if (ticks % 20 == 0 && !isOffline()) {
            setPing(getPlayer().getPing());
        }

        // Autosave every minute
        if (ticks % 1200 == 0) {
            PlayersService.i().save(this);
        }

        // Check friends cooldown
        if (ticks % 100 == 0) {
            FriendsService.i().checkCooldown(this);
        }

        // Status message
        if (statusMsg != null) {
            MessageUtil.sendTitle("", ChatColor.YELLOW + statusMsg, this);
        }

        // Scheduled tasks
        Set<Runnable> toRun = new HashSet<>();
        scheduledTasks.entrySet().removeIf(entry -> {
            if (entry.getValue().decrementAndGet() <= 0) {
                toRun.add(entry.getKey());
                return true;
            }
            return false;
        });

        toRun.forEach(Runnable::run);
    }

    /**
     * Set the player's status message
     *
     * @param statusMsg The message to set
     */
    public void setStatusMsg(String statusMsg) {
        this.statusMsg = statusMsg;

        if (statusMsg == null) {
            MessageUtil.sendTitle("", "", this);
        }
    }

    // Methods that should be overridden

    /**
     * Called once on load
     */
    protected void onLoad() {
    }

    /**
     * Called once on join after {@link #onLoad()}
     *
     * @param networkWide Whether the player joined the network (true) or
     *                    moved from another server (false)
     */
    public void onJoin(boolean networkWide) {
    }

    /**
     * Called once on quit
     */
    public void onQuit() {
    }

    /**
     * Called every game tick
     *
     * @param ticks The number of ticks elapsed since the player joined
     */
    protected void tick(long ticks) {
    }

    /**
     * Called to generate the chat hover info text
     *
     * @return Should return the hover info map or null
     */
    public Map<String, String> getChatHoverInfo() {
        return null;
    }

    /**
     * Called before teleporting the player to another server to save their current state.
     * There may be some delay between the call of this method and the actual teleportation.
     *
     * @param serverShutdown Whether this called on server shutdown
     */
    public void saveTpState(boolean serverShutdown) {
    }

    /**
     * Process a number of server listing websites votes
     *
     * @param count The number of votes
     */
    public void processVotes(int count) {
    }

    public void playSound(Sound sound) {
        playSound(sound, 1, 1);
    }

    public void playSound(Sound sound, double volume, double pitch) {
        if (isOffline())
            return;

        playSound(sound, getPlayer().getLocation(), volume, pitch);
    }

    public void playSound(Sound sound, Location location, double volume, double pitch) {
        if (isOffline())
            return;

        Player p = getPlayer();
        p.playSound(location, sound, (float) volume, (float) pitch);
    }

    public void clickSound() {
        playSound(Sound.UI_BUTTON_CLICK, 0.5, 1);
    }

    public void popSound() {
        playSound(Sound.ENTITY_CHICKEN_EGG, 0.5, 1);
    }

    public void noSound() {
        playSound(Sound.BLOCK_NOTE_BLOCK_BASS, 1.5, 0.5);
    }

    public void levelUpSound() {
        playSound(Sound.ENTITY_PLAYER_LEVELUP, 0.5, 1);
    }

    public void expSound() {
        playSound(Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 0.5, 1);
    }

    /**
     * Schedule a task to be executed after a set amount of ticks
     *
     * @param delay The delay in ticks
     * @param task  The task to execute
     */
    public void schedule(int delay, Runnable task) {
        scheduledTasks.put(task, new AtomicInteger(delay));
    }

    public void updateFriends() {
    }
}
