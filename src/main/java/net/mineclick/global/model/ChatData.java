package net.mineclick.global.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

@Getter
@Setter
@NoArgsConstructor
public class ChatData {
    private static final int MAX_HISTORY = 100;

    private AtomicBoolean disabled = new AtomicBoolean();
    private AtomicBoolean privateFromFriendsOnly = new AtomicBoolean();
    private boolean staffChat = false;
    private List<String> history = new ArrayList<>();

    private transient String lastMessage;

    public void addHistory(String message) {
        while (!history.isEmpty() && history.size() > MAX_HISTORY) {
            history.remove(0);
        }

        history.add("[" + new Date() + "] " + message);
    }
}
