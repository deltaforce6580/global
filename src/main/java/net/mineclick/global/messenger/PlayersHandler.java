package net.mineclick.global.messenger;

import com.google.gson.JsonObject;
import lombok.Getter;
import lombok.Setter;
import net.mineclick.core.messenger.Action;
import net.mineclick.core.messenger.Message;
import net.mineclick.core.messenger.MessageName;
import net.mineclick.global.GlobalPlugin;
import net.mineclick.global.model.PlayerModel;

import java.util.UUID;
import java.util.function.Consumer;
import java.util.logging.Level;

@Setter
@Getter
@MessageName("players")
public class PlayersHandler extends Message {
    private UUID uuid;
    private JsonObject playerModel;

    public static <T extends PlayerModel> void get(UUID uuid, boolean createNew, Class<T> tClass, Consumer<T> consumer) {
        PlayersHandler handler = new PlayersHandler();
        handler.setUuid(uuid);
        handler.setResponseConsumer(message -> {
            if (message != null) {
                switch (message.getResponse()) {
                    case OK:
                        try {
                            T model = GlobalPlugin.i().getGson().fromJson(((PlayersHandler) message).playerModel, tClass);
                            consumer.accept(model);
                        } catch (Exception e) {
                            if (consumer != null) {
                                consumer.accept(null);
                            }

                            e.printStackTrace();
                            GlobalPlugin.i().getLogger().log(Level.SEVERE, "Could not PARSE player data (" + uuid + ")", e);
                            return;
                        }
                        break;
                    case NOT_FOUND:
                        consumer.accept(createNew ? PlayerModel.createNew(uuid, tClass) : null);
                        break;
                    default:
                        consumer.accept(null);
                        GlobalPlugin.i().getLogger().severe("Could not LOAD player data (" + uuid + "). Backend error");
                }
            } else {
                consumer.accept(null);
                GlobalPlugin.i().getLogger().severe("Could not LOAD player data: " + uuid);
            }
        });

        handler.send(Action.GET);
    }

    public static <T extends PlayerModel> PlayersHandler save(T playerModel) {
        PlayersHandler handler = new PlayersHandler();
        handler.setUuid(playerModel.getUuid());
        handler.setPlayerModel(GlobalPlugin.i().getGson().toJsonTree(playerModel).getAsJsonObject());

        handler.send(Action.POST);
        return handler;
    }
}
