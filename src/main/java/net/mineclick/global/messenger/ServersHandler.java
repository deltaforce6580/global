package net.mineclick.global.messenger;

import lombok.Setter;
import net.mineclick.core.messenger.Message;
import net.mineclick.core.messenger.MessageName;
import net.mineclick.global.model.ServerModel;
import net.mineclick.global.service.ServersService;

@Setter
@MessageName("servers")
public class ServersHandler extends Message {
    private ServerModel server;

    @Override
    public void onReceive() {
        ServersService.i().handleServer(server);
    }
}
