package net.mineclick.global.messenger;

import lombok.Getter;
import lombok.Setter;
import net.mineclick.core.messenger.Action;
import net.mineclick.core.messenger.Message;
import net.mineclick.core.messenger.MessageName;
import net.mineclick.core.messenger.Response;
import net.mineclick.global.model.OffenceData;
import net.mineclick.global.service.PlayersService;
import org.bukkit.Bukkit;

import java.time.Instant;
import java.util.UUID;

@Setter
@Getter
@MessageName("offences")
public class OffencesHandler extends Message {
    private UUID uuid;
    private OffenceData offenceData;

    @Override
    public void onReceive() {
        if (getAction().equals(Action.POST)) {
            if (Bukkit.getPlayer(uuid) != null) {
                PlayersService.i().edit(uuid, playerModel -> playerModel.getOffences().add(offenceData));

                send(Response.OK);
            }
        } else if (getAction().equals(Action.DELETE)) {
            if (Bukkit.getPlayer(uuid) != null) {
                PlayersService.i().edit(uuid, playerModel -> {
                    for (OffenceData offence : playerModel.getOffences()) {
                        if (offence.isStillPunished() && offence.getPunishment().equals(offenceData.getPunishment())) {
                            offence.setPardonedBy(offenceData.getPardonedBy());
                            offence.setPardonedOn(Instant.now());
                        }
                    }
                });

                send(Response.OK);
            }
        }
    }
}
