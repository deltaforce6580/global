package net.mineclick.global.messenger;

import net.mineclick.core.messenger.Action;
import net.mineclick.core.messenger.Message;
import net.mineclick.core.messenger.MessageName;
import net.mineclick.core.messenger.Response;
import net.mineclick.global.service.VotesService;

@MessageName("votes")
public class VotesHandler extends Message {
    private String username;

    @Override
    public void onReceive() {
        if (getAction().equals(Action.UPDATE) && VotesService.i().handleVote(username)) {
            send(Response.OK);
        }
    }
}
