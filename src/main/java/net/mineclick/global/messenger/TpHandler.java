package net.mineclick.global.messenger;

import lombok.Setter;
import net.mineclick.core.messenger.Message;
import net.mineclick.core.messenger.MessageName;

import java.util.UUID;

@Setter
@MessageName("tp")
public class TpHandler extends Message {
    private UUID uuid;
    private String serverId;
}
