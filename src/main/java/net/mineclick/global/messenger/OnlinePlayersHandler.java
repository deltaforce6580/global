package net.mineclick.global.messenger;

import lombok.Getter;
import lombok.Setter;
import net.mineclick.core.messenger.Action;
import net.mineclick.core.messenger.Message;
import net.mineclick.core.messenger.MessageName;
import net.mineclick.global.model.PlayerId;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

@Getter
@Setter
@MessageName("onlinePlayers")
public class OnlinePlayersHandler extends Message {
    private List<PlayerId> players = new ArrayList<>();

    public static void setOnline(PlayerId player) {
        player.setOnline(true);
        OnlinePlayersHandler handler = new OnlinePlayersHandler();
        handler.players.add(player);
        handler.send(Action.POST);
    }

    public static void get(Consumer<List<PlayerId>> consumer) {
        OnlinePlayersHandler handler = new OnlinePlayersHandler();
        handler.setResponseConsumer(message -> {
            if (message != null) {
                consumer.accept(((OnlinePlayersHandler) message).players);
            } else {
                consumer.accept(null);
            }
        });
        handler.send(Action.GET);
    }
}
