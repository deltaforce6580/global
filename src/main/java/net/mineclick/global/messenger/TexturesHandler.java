package net.mineclick.global.messenger;

import lombok.Data;
import net.mineclick.core.messenger.Message;
import net.mineclick.core.messenger.MessageName;

import java.util.Map;
import java.util.UUID;

@Data
@MessageName("textures")
public class TexturesHandler extends Message {
    private Map<UUID, String> textures;
}