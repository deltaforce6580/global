package net.mineclick.global.messenger;

import lombok.Setter;
import net.mineclick.core.messenger.Message;
import net.mineclick.core.messenger.MessageName;
import net.mineclick.core.messenger.Response;
import net.mineclick.global.model.ChatSenderData;
import net.mineclick.global.service.ChatService;
import net.mineclick.global.service.PlayersService;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;


@Setter
@MessageName("chat")
public class ChatHandler extends Message {
    private ChatType chatType;
    private ChatSenderData senderData;
    private String recipientName;
    private String message;
    private String secondMessage;
    private boolean important;

    @Override
    public void onReceive() {
        switch (chatType) {
            case PUBLIC:
                ChatService.i().handlePublicChat(senderData, message);
                break;
            case PRIVATE:
                Player recipient = Bukkit.getPlayerExact(recipientName);
                if (recipient != null) {
                    PlayersService.i().get(recipient.getUniqueId(), to -> {
                        boolean success = ChatService.i().handlePrivateChat(senderData, message, to);
                        send(success ? Response.OK : Response.ERROR);
                    });
                }
                break;
            case BROADCAST:
                ChatService.i().handleBroadcast(message, secondMessage, important);
                break;
            case STAFF:
                ChatService.i().handleStaffChat(senderData, message);
                break;
        }
    }

    @Override
    public void onResponse(Message message) {
        if (chatType.equals(ChatType.PRIVATE) && !message.isOk()) {
            PlayersService.i().get(senderData.getUuid(), playerModel -> {
                Player player = playerModel.getPlayer();
                if (player != null) {
                    player.sendMessage(ChatColor.RED + "Can't send private messages to " + recipientName);
                }
            });
        }
    }

    @Override
    public void onTimeout() {
        if (chatType.equals(ChatType.PRIVATE)) {
            PlayersService.i().get(senderData.getUuid(), playerModel -> {
                Player player = playerModel.getPlayer();
                if (player != null) {
                    player.sendMessage(ChatColor.RED + "Player " + recipientName + " is not found");
                }
            });
        }
    }

    public enum ChatType {
        PUBLIC, PRIVATE, BROADCAST, STAFF
    }
}
