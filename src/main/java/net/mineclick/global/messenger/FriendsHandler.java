package net.mineclick.global.messenger;

import lombok.Getter;
import lombok.Setter;
import net.mineclick.core.messenger.Message;
import net.mineclick.core.messenger.MessageName;
import net.mineclick.core.messenger.Response;
import net.mineclick.global.model.ChatSenderData;
import net.mineclick.global.service.FriendsService;

import java.util.UUID;

@Getter
@Setter
@MessageName("friends")
public class FriendsHandler extends Message {
    private ActionType actionType;
    private ChatSenderData senderData;
    private UUID recipient;

    @Override
    public void onReceive() {
        switch (actionType) {
            case REQUEST:
                if (FriendsService.i().handleRequest(senderData, recipient)) {
                    send(Response.OK);
                }
                break;
            case ACCEPT:
            case DENY:
                if (FriendsService.i().handleAnswer(senderData, recipient, actionType == ActionType.ACCEPT)) {
                    send(Response.OK);
                }
                break;
            case CANCEL:
                if (FriendsService.i().handleCancel(senderData, recipient)) {
                    send(Response.OK);
                }
                break;
            case REMOVE:
                if (FriendsService.i().handleRemove(senderData, recipient)) {
                    send(Response.OK);
                }
                break;
        }
    }

    public enum ActionType {
        REQUEST,
        ACCEPT,
        CANCEL,
        DENY,
        REMOVE
    }
}
