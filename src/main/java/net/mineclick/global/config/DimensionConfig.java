package net.mineclick.global.config;

import lombok.Getter;
import lombok.Setter;
import net.mineclick.global.Constants;
import net.mineclick.global.GlobalPlugin;
import net.mineclick.global.util.BigNumber;
import net.mineclick.global.util.ChunkPreload;
import net.mineclick.global.util.Formatter;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.event.inventory.InventoryType;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class DimensionConfig extends ConfigFile {
    @Getter
    private static List<DimensionConfig> dimensionList = new ArrayList<>();
    private final List<IslandConfig> islands = new ArrayList<>();
    private int id = 0;
    @Save
    private String skin;
    @Save
    private int preDimension;
    @Save
    private int preAscends;
    @Save
    private BigNumber minGold;
    @Save
    private BigNumber maxGold;
    @Save
    private double multiplier;
    @Save
    private String mineshaftBlockMaterial;
    @Save
    private int mineshaftClicksPerBlock;

    public DimensionConfig(File file) {
        super(file);

        GlobalPlugin.i().getLogger().info("Loading dimension config: " + getName());
        islands.addAll(loadAll("dimensions/" + file.getName(), IslandConfig.class));
        islands.forEach(islandConfig -> {
            islandConfig.setDimension(this);

            if (!Constants.QUICK_LOAD) {
                ChunkPreload.preload(islandConfig.getSpawn().toLocation().getChunk());
            }
        });
    }

    public static void loadFiles() {
        Bukkit.getOnlinePlayers().forEach(p -> {
            if (p.getOpenInventory().getTopInventory().getType().equals(InventoryType.CHEST)) {
                p.closeInventory();
                p.sendMessage(ChatColor.RED + "Reloading config so had to close your inventory");
            }
        });

        long time = System.currentTimeMillis();
        GlobalPlugin.i().getLogger().info("Loading dimensions...");
        dimensionList.clear();
        dimensionList.addAll(loadAll("dimensions", DimensionConfig.class));
        GlobalPlugin.i().getLogger().info("Loaded " + dimensionList.size() + " dimensions (" + Formatter.duration(System.currentTimeMillis() - time) + "s)");

        for (int i = 0; i < dimensionList.size(); i++) {
            dimensionList.get(i).setId(i);
        }
    }

    public static DimensionConfig getById(int id) {
        return id < dimensionList.size() ? dimensionList.get(id) : null;
    }
}
