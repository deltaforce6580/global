package net.mineclick.global.config;

import lombok.Getter;
import lombok.Setter;
import net.mineclick.global.Constants;
import net.mineclick.global.config.field.MineRegionConfig;
import net.mineclick.global.config.field.UpgradeConfig;
import net.mineclick.global.util.BigNumber;
import net.mineclick.global.util.location.ConfigLocation;
import net.mineclick.global.util.location.Region;
import net.mineclick.global.util.schematic.Schematic;

import java.io.File;
import java.util.ArrayList;

@Getter
@Setter
public class BuildingConfig extends ConfigFile {
    private IslandConfig island;
    @Save
    private Region region;
    @Save
    private ConfigLocation npcSpawn;
    @Save
    private ArrayList<String> names;
    @Save
    private ArrayList<UpgradeConfig> upgrades;
    @Save
    private ArrayList<BigNumber> costs;
    @Save
    private ArrayList<MineRegionConfig> extraMineRegions;
    @Save
    private ArrayList<ConfigLocation> extraNpcSpawns;
    @Save
    private ArrayList<Schematic> schematics;

    public BuildingConfig(File dir) {
        super(dir);

        if (!Constants.QUICK_LOAD && schematics != null && region != null) {
            schematics.forEach(schematic -> schematic.load(region.getMin(), false));
        }
    }

    public boolean includeDefaultFields() {
        return false;
    }
}
