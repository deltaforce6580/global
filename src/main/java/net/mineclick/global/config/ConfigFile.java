package net.mineclick.global.config;

import lombok.Getter;
import lombok.Setter;
import net.mineclick.global.GlobalPlugin;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.ParameterizedType;
import java.util.*;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.stream.Collectors;

@Getter
@Setter
public abstract class ConfigFile {
    private final FileConfiguration config;

    private final String dirPath;
    private final String localDirPath;
    @Save
    private String name;
    @Save
    private String description;

    public ConfigFile(File dir) {
        dirPath = dir.getPath();
        localDirPath = dirPath.replace(GlobalPlugin.i().getConfigDir() + "/", "");

        dir.mkdirs();
        File configFile = new File(dir.getPath() + "/config.yml");
        try {
            configFile.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
            config = null;
            return;
        }

        config = YamlConfiguration.loadConfiguration(configFile);
        load();
        if (name == null) {
            name = dir.getName();
        }
    }

    public static File createNewUniqueFile(String dirPath, String name) {
        int i = 0;
        File file;
        do {
            String postfix = String.valueOf(i);
            file = new File(dirPath + "/" + name + postfix);
            i++;
        } while (file.exists() && i < 100);

        file.mkdirs();
        return file;
    }

    public static <T extends ConfigFile> List<T> loadAll(String path, Class<T> tClass) {
        return loadAll(new File(GlobalPlugin.i().getConfigDir() + "/" + path), tClass);
    }

    public static <T extends ConfigFile> List<T> loadAll(File dir, Class<T> tClass) {
        List<T> list = new ArrayList<>();

        if (!dir.exists()) {
            dir.mkdirs();
            return list;
        }

        File[] files = dir.listFiles();
        if (files == null) {
            return list;
        }
        List<File> fileList = Arrays.asList(files);
        fileList.sort(Comparator.comparing(File::getName));
        try {
            Constructor<T> constructor = tClass.getDeclaredConstructor(File.class);

            for (File file : fileList) {
                if (file.isDirectory()) {
                    list.add(constructor.newInstance(file));
                }
            }
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            e.printStackTrace();
        }

        return list;
    }

    public void save() {
        forAllSaveFields((clazz, field) -> {
            Save save = field.getAnnotation(Save.class);
            String path = save.path().isEmpty() ? "" : save.path() + ".";
            try {
                Object object = field.get(this);
                if (object == null)
                    return;

                if (field.getType().isEnum()) {
                    config.set(path + field.getName(), ((Enum) object).name());
                    return;
                }

                if (object instanceof ArrayList) {
                    if (((ArrayList) object).isEmpty())
                        return;

                    Class<?> fieldType = (Class) ((ParameterizedType) field.getGenericType()).getActualTypeArguments()[0];

                    if (fieldType.getDeclaredAnnotation(SaveConstructor.class) != null) {
                        SaveConstructor saveConstructor = fieldType.getDeclaredAnnotation(SaveConstructor.class);
                        if (saveConstructor.args().length == 1) {
                            ArrayList<String> list = new ArrayList<>();
                            for (Object o : ((ArrayList) object)) {
                                if (o == null)
                                    continue;

                                Field f = o.getClass().getDeclaredField(saveConstructor.args()[0]);
                                f.setAccessible(true);
                                list.add(String.valueOf(f.get(o)));
                            }

                            config.set(path + field.getName(), list);
                        } else {
                            ArrayList<Map<?, ?>> list = new ArrayList<>();
                            for (Object o : ((ArrayList) object)) {
                                if (o == null)
                                    continue;

                                Map<String, String> map = new LinkedHashMap<>();

                                for (String stringField : saveConstructor.args()) {
                                    Field f = o.getClass().getDeclaredField(stringField);
                                    f.setAccessible(true);

                                    String s = String.valueOf(f.get(o));
                                    map.put(stringField, s);
                                }

                                list.add(map);
                            }

                            config.set(path + field.getName(), list);
                        }
                    } else {
                        config.set(path + field.getName(), object);
                    }
                } else {
                    Class<?> type = field.getType();
                    if (type.getDeclaredAnnotation(SaveConstructor.class) != null) {
                        SaveConstructor saveConstructor = type.getDeclaredAnnotation(SaveConstructor.class);

                        for (String stringField : saveConstructor.args()) {
                            Field f = object.getClass().getDeclaredField(stringField);
                            f.setAccessible(true);

                            String s = String.valueOf(f.get(object));
                            if (saveConstructor.args().length == 1) {
                                config.set(path + field.getName(), s);
                            } else {
                                config.set(path + field.getName() + "." + stringField, s);
                            }
                        }
                    } else {
                        config.set(path + field.getName(), object);
                    }
                }
            } catch (IllegalAccessException | NoSuchFieldException e) {
                e.printStackTrace();
            }
        });

        try {
            config.save(new File(dirPath + "/config.yml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void load() {
        forAllSaveFields(field -> {
            Save save = field.getAnnotation(Save.class);
            String path = save.path().isEmpty() ? "" : save.path() + ".";
            if (config.contains(path + field.getName())) {
                try {
                    Class<?> type = field.getType();

                    if (type.isEnum()) {
                        field.set(this, type.getDeclaredMethod("valueOf", String.class).invoke(null, config.getString(path + field.getName())));
                        return;
                    }

                    if (type.isAssignableFrom(ArrayList.class)) {
                        Class<?> fieldType = (Class) ((ParameterizedType) field.getGenericType()).getActualTypeArguments()[0];
                        if (fieldType.getDeclaredAnnotation(SaveConstructor.class) != null) {
                            SaveConstructor saveConstructor = fieldType.getDeclaredAnnotation(SaveConstructor.class);

                            Class[] classes = new Class[saveConstructor.args().length];
                            Arrays.fill(classes, String.class);
                            Constructor constructor = fieldType.getDeclaredConstructor(classes);

                            List<Object> list = new ArrayList<>();
                            if (saveConstructor.args().length == 1) {
                                List<String> configList = config.getStringList(path + field.getName());
                                for (String arg : configList) {
                                    list.add(constructor.newInstance(arg));
                                }
                            } else {
                                List<Map<?, ?>> mapList = config.getMapList(path + field.getName());
                                for (Map<?, ?> map : mapList) {
                                    String[] intargs = Arrays.stream(saveConstructor.args()).map(s -> ((String) map.get(s))).collect(Collectors.toList()).toArray(new String[0]);
                                    list.add(constructor.newInstance(intargs));
                                }
                            }
                            field.set(this, list);
                        } else {
                            field.set(this, config.get(path + field.getName()));
                        }
                    } else {
                        Class<?> fieldType = field.getType();
                        if (fieldType.getDeclaredAnnotation(SaveConstructor.class) != null) {
                            SaveConstructor saveConstructor = fieldType.getDeclaredAnnotation(SaveConstructor.class);

                            Class[] classes = new Class[saveConstructor.args().length];
                            Arrays.fill(classes, String.class);

                            Constructor constructor = type.getDeclaredConstructor(classes);

                            if (saveConstructor.args().length == 1) {
                                field.set(this, constructor.newInstance(String.valueOf(config.get(path + field.getName()))));
                            } else {
                                String[] intargs = Arrays.stream(saveConstructor.args()).map(s -> String.valueOf(config.get(path + field.getName() + "." + s))).collect(Collectors.toList()).toArray(new String[0]);
                                field.set(this, constructor.newInstance(intargs));
                            }
                        } else {
                            field.set(this, config.get(path + field.getName()));
                        }
                    }
                } catch (IllegalAccessException | NoSuchMethodException | InvocationTargetException | InstantiationException e) {
                    e.printStackTrace();
                }
            } else if (field.getType().isAssignableFrom(ArrayList.class)) {
                try {
                    field.set(this, new ArrayList<>());
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void forAllSaveFields(BiConsumer<Class, Field> consumer) {
        Class clazz = this.getClass();
        do {
            if (includeDefaultFields() || !clazz.equals(ConfigFile.class)) {
                for (Field field : clazz.getDeclaredFields()) {
                    if (field.getAnnotation(Save.class) != null) {
                        field.setAccessible(true);
                        consumer.accept(clazz, field);
                    }
                }
            }

            clazz = clazz.getSuperclass();
        } while (clazz != null);
    }

    public void forAllSaveFields(Consumer<Field> consumer) {
        Class clazz = this.getClass();
        do {
            if (includeDefaultFields() || !clazz.equals(ConfigFile.class)) {
                for (Field field : clazz.getDeclaredFields()) {
                    if (field.getAnnotation(Save.class) != null) {
                        field.setAccessible(true);
                        consumer.accept(field);
                    }
                }
            }

            clazz = clazz.getSuperclass();
        } while (clazz != null);
    }

    public boolean includeDefaultFields() {
        return true;
    }
}
