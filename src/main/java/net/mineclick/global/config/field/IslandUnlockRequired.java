package net.mineclick.global.config.field;

import lombok.Getter;
import net.mineclick.global.config.SaveConstructor;

@Getter
@SaveConstructor(args = {"mob", "lvl"})
public class IslandUnlockRequired {
    private final String mob;
    private final String lvl;
    private final MobType mobType;
    private final int level;

    public IslandUnlockRequired(String mob, String lvl) {
        this.mob = mob;
        this.lvl = lvl;

        this.mobType = MobType.valueOf(mob);
        this.level = Integer.parseInt(lvl);
    }

    public IslandUnlockRequired(MobType mobType, int level) {
        mob = mobType.toString();
        lvl = String.valueOf(level);

        this.mobType = mobType;
        this.level = level;
    }

    @Override
    public String toString() {
        return mob + ":" + lvl;
    }

    public enum MobType {
        ZOMBIE,
        SKELETON,
        SPIDER,
        CREEPER,
        PIGMAN,
        SLIME,
        SNOWMAN,
        ENDERMAN,
        BLAZE,
        GOLEM
    }
}
