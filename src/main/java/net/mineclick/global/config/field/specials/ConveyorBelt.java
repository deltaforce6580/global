package net.mineclick.global.config.field.specials;

import lombok.Getter;
import net.mineclick.global.config.SaveConstructor;
import net.mineclick.global.model.PlayerModel;
import net.mineclick.global.util.location.ConfigLocation;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

@Getter
@SaveConstructor(args = {"path", "skin", "buildingId", "buildingLevel"})
public class ConveyorBelt {
    private final String path;
    private final String skin;
    private final String buildingId; //Building id and level on which the special appears on
    private final String buildingLevel;

    private List<ConfigLocation> pathLocations = new ArrayList<>();

    public ConveyorBelt(String path, String skin, String buildingId, String buildingLevel) {
        this.path = path;
        this.skin = skin;
        this.buildingId = buildingId;
        this.buildingLevel = buildingLevel;

        path = path.replace("[", "").replace("]", "");
        for (String node : path.split(", ")) {
            pathLocations.add(new ConfigLocation(node));
        }
    }

    public ConveyorBelt(List<ConfigLocation> pathLocations, int buildingId, int buildingLevel) {
        this.pathLocations = pathLocations;
        this.skin = "";
        this.buildingId = buildingId + "";
        this.buildingLevel = buildingLevel + "";

        this.path = pathLocations.toString();
    }

    public void visualize(PlayerModel player) {
        for (int i = 0; i < pathLocations.size(); i++) {
            pathLocations.get(i).visualize(player, (i == 0) ? Color.GREEN : ((i == (pathLocations.size() - 1)) ? Color.RED : Color.ORANGE));
        }
    }

    public void stopVisualizing(PlayerModel player) {
        pathLocations.forEach(l -> l.stopVisualizing(player));
    }
}
