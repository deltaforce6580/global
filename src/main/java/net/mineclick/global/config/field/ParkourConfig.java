package net.mineclick.global.config.field;

import lombok.Getter;
import net.mineclick.global.config.SaveConstructor;
import net.mineclick.global.util.location.ConfigLocation;
import net.mineclick.global.util.location.LocationParser;
import net.mineclick.global.util.location.Region;
import org.bukkit.Location;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@SaveConstructor(args = {"spawn", "npcSpawn", "path"})
public class ParkourConfig {
    private final String spawn;
    private final String npcSpawn;
    private final String path;

    private final ConfigLocation spawnLocation;
    private final ConfigLocation npcSpawnLocation;
    private List<ConfigLocation> pathList = new ArrayList<>();
    private int lowestY;
    private Region startRegion;

    public ParkourConfig(String spawn, String npcSpawn, String path) {
        this.spawn = spawn;
        this.npcSpawn = npcSpawn;
        this.path = path;

        spawnLocation = new ConfigLocation(spawn);
        npcSpawnLocation = new ConfigLocation(npcSpawn);

        path = path.replace("[", "").replace("]", "");
        for (String node : path.split(", ")) {
            pathList.add(new ConfigLocation(node));
        }

        prepare();
    }

    public ParkourConfig(Location spawn, Location npcSpawn, List<Location> path) {
        this.spawn = LocationParser.toString(spawn);
        this.npcSpawn = LocationParser.toString(npcSpawn);
        List<ConfigLocation> configLocations = path.stream().map(ConfigLocation::new).collect(Collectors.toList());
        this.path = configLocations.toString();

        spawnLocation = new ConfigLocation(spawn);
        npcSpawnLocation = new ConfigLocation(npcSpawn);
        pathList = configLocations;

        prepare();
    }

    private void prepare() {
        if (!pathList.isEmpty()) {
            List<Location> locations = pathList.stream().map(ConfigLocation::toLocation).collect(Collectors.toList());
            lowestY = locations.stream().mapToInt(Location::getBlockY).min().orElse(10) - 10;

            Location loc = locations.get(0);
            startRegion = new Region(loc.clone().add(-3, -2, -3), loc.clone().add(3, 2, 3));
        }
    }

    @Override
    public String toString() {
        return spawn;
    }
}
