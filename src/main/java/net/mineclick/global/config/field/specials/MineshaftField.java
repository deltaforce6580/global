package net.mineclick.global.config.field.specials;

import lombok.Getter;
import net.mineclick.global.config.SaveConstructor;
import net.mineclick.global.model.PlayerModel;
import net.mineclick.global.util.location.ConfigLocation;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

@Getter
@SaveConstructor(args = {"blocks", "spawn"})
public class MineshaftField {
    private final String blocks;
    private final ConfigLocation spawn;

    private List<ConfigLocation> blockLocations = new ArrayList<>();

    public MineshaftField(String blocks, String spawn) {
        this.blocks = blocks;
        this.spawn = new ConfigLocation(spawn);

        blocks = blocks.replace("[", "").replace("]", "");
        for (String node : blocks.split(", ")) {
            blockLocations.add(new ConfigLocation(node));
        }
    }

    public MineshaftField(List<ConfigLocation> blockLocations, ConfigLocation spawn) {
        this.blockLocations = blockLocations;
        this.spawn = spawn;

        this.blocks = blockLocations.toString();
    }

    public void visualize(PlayerModel player) {
        if (spawn != null) {
            spawn.visualize(player, Color.RED);
        }
        blockLocations.forEach(b -> b.visualize(player, Color.ORANGE));
    }

    public void stopVisualizing(PlayerModel player) {
        if (spawn != null) {
            spawn.stopVisualizing(player);
        }
        blockLocations.forEach(l -> l.stopVisualizing(player));
    }
}
