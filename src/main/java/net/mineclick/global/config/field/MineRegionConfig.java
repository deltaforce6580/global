package net.mineclick.global.config.field;

import lombok.Getter;
import net.mineclick.global.GlobalPlugin;
import net.mineclick.global.config.SaveConstructor;
import net.mineclick.global.model.PlayerModel;
import net.mineclick.global.util.location.ConfigLocation;
import net.mineclick.global.util.location.LocationParser;
import net.mineclick.global.util.location.Region;
import net.minecraft.world.entity.Mob;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;

import java.awt.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@SaveConstructor(args = {"loc1", "loc2", "block", "item", "path"})
public class MineRegionConfig {
    private final String loc1;
    private final String loc2;
    private final String block;
    private final String item;
    private final String path; // TODO path is not used anymore

    private final Region region;
    private final Material blockMaterial;
    private final Material itemMaterial;
    private final List<ConfigLocation> visualizingLocations = new ArrayList<>();
    private List<Location> pathList = new ArrayList<>();

    public MineRegionConfig(String loc1, String loc2, String block, String item, String path) {
        this.loc1 = loc1;
        this.loc2 = loc2;
        this.block = block;
        this.item = item;
        this.path = path;

        region = new Region(loc1, loc2);
        blockMaterial = Material.getMaterial(block);
        itemMaterial = Material.getMaterial(item);

        path = path.replace("[", "").replace("]", "");
        for (String node : path.split(", ")) {
            pathList.add(LocationParser.parse(node));
        }
    }

    public MineRegionConfig(Location loc1, Location loc2, Material blockMaterial, Material itemMaterial, List<Location> path) {
        this.loc1 = LocationParser.toString(loc1);
        this.loc2 = LocationParser.toString(loc2);
        this.block = blockMaterial.name();
        this.item = itemMaterial.name();
        this.path = path.stream().map(ConfigLocation::new).collect(Collectors.toList()).toString();

        region = new Region(loc1, loc2);
        this.blockMaterial = blockMaterial;
        this.itemMaterial = itemMaterial;
        pathList = path;
    }

    public List<Block> getBlocks() {
        return region.getBlocks(blockMaterial);
    }

    public Block getRandomBlock() {
        List<Block> blocks = getBlocks();
        if (blocks.isEmpty()) return null;
        return blocks.get(GlobalPlugin.random.nextInt(blocks.size()));
    }

    public Location getRandomAccessibleBlock(Mob entity) {
        List<Block> blocks = region.getBlocks(blockMaterial);
        Collections.shuffle(blocks);

        for (Block b : blocks) {
            boolean found = false;
            for (int i = 0; i < 4; i++) {
                if (b.getRelative(BlockFace.values()[i]).getType().equals(Material.AIR)) {
                    b = b.getRelative(BlockFace.values()[i]);

                    while (b.getRelative(BlockFace.DOWN).getType().equals(Material.AIR) && b.getLocation().getY() > 0) {
                        b = b.getRelative(BlockFace.DOWN);
                    }
                    found = true;
                    break;
                }
            }

            if (found) {
                Location l = b.getLocation().add(0.5, 0, 0.5);
                if (entity.getNavigation().createPath(l.getX(), l.getY(), l.getZ(), 1) != null)
                    return l;
            }
        }

        return null;
    }

    @Override
    public String toString() {
        return blockMaterial.toString();
    }

    public void visualizePath(PlayerModel player) {
        stopVisualizing(player);

        visualizingLocations.addAll(pathList.stream().map(ConfigLocation::new).collect(Collectors.toList()));
        for (int i = 0; i < visualizingLocations.size(); i++) {
            visualizingLocations.get(i).visualize(player, (i == 0) ? Color.GREEN : ((i == (visualizingLocations.size() - 1)) ? Color.RED : Color.ORANGE));
        }
    }

    public void stopVisualizing(PlayerModel player) {
        visualizingLocations.forEach(l -> l.stopVisualizing(player));
        visualizingLocations.clear();
    }
}
