package net.mineclick.global.config;

import lombok.Getter;
import lombok.Setter;
import net.mineclick.global.GlobalPlugin;
import net.mineclick.global.config.field.specials.MineshaftField;
import net.mineclick.global.util.Formatter;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.event.inventory.InventoryType;

import java.io.File;

@Getter
@Setter
public class MineshaftConfig extends ConfigFile {
    @Getter
    private static MineshaftConfig mineshaft;

    @Save
    private MineshaftField mineshaftField;

    public MineshaftConfig(File dir) {
        super(dir);
    }

    public static void loadFromFile() {
        Bukkit.getOnlinePlayers().forEach(p -> {
            if (p.getOpenInventory().getTopInventory().getType().equals(InventoryType.CHEST)) {
                p.closeInventory();
                p.sendMessage(ChatColor.RED + "Reloading config so had to close your inventory");
            }
        });

        long time = System.currentTimeMillis();
        mineshaft = new MineshaftConfig(new File(GlobalPlugin.i().getConfigDir() + "/mineshaft"));
        GlobalPlugin.i().getLogger().info("Loaded mineshaft (" + Formatter.duration(System.currentTimeMillis() - time) + "s)");
    }

    public boolean includeDefaultFields() {
        return false;
    }
}
