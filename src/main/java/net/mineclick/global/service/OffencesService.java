package net.mineclick.global.service;

import net.mineclick.core.messenger.Action;
import net.mineclick.global.messenger.ChatHandler;
import net.mineclick.global.messenger.OffencesHandler;
import net.mineclick.global.model.OffenceData;
import net.mineclick.global.model.PlayerModel;
import net.mineclick.global.type.OffenceType;
import net.mineclick.global.type.PunishmentType;
import net.mineclick.global.util.Pair;
import net.mineclick.global.util.SingletonInit;
import org.bukkit.ChatColor;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Comparator;

@SingletonInit
public class OffencesService {
    private static OffencesService i;

    public static OffencesService i() {
        return i == null ? i = new OffencesService() : i;
    }

    /**
     * Punish a player for a specific reason
     *
     * @param punisher    The punisher
     * @param offender    The offender
     * @param offenceType The type of offence
     * @param punishment  Punishment type and duration. Can be null
     */
    public void punish(PlayerModel punisher, PlayerModel offender, OffenceType offenceType, Pair<PunishmentType, Integer> punishment) {
        if (!offender.isOffline()) {
            OffenceData offenceData = buildOffenceData(punisher, offenceType, punishment);

            // Save the offence
            offender.getOffences().add(offenceData);

            // Apply the punishment
            if (punishment != null) {
                punishment.key().getApply().accept(offender, offenceData);
            }
        } else {
            OffenceData offenceData = buildOffenceData(punisher, offenceType, punishment);

            OffencesHandler handler = new OffencesHandler();
            handler.setUuid(offender.getUuid());
            handler.setOffenceData(offenceData);
            handler.send(Action.POST);
            handler.setResponseConsumer(message -> {
                if (message == null || !message.isOk()) {
                    PlayersService.i().edit(offender.getUuid(), player -> player.getOffences().add(offenceData));
                }
            });
        }

        if (punishment != null) {
            sendMessage(punisher, offender.getName(), punishment.key(), true);
        }
    }

    /**
     * Forgive a player and lift the punishment
     *
     * @param staff          The player who forgave the player
     * @param offender       The offender
     * @param punishmentType The punishment type
     */
    public void pardon(PlayerModel staff, PlayerModel offender, PunishmentType punishmentType, Runnable callback) {
        if (!offender.isOffline()) {
            for (OffenceData offence : offender.getOffences()) {
                if (offence.isStillPunished() && offence.getPunishment().equals(punishmentType)) {
                    offence.setPardonedBy(staff.getName());
                    offence.setPardonedOn(Instant.now());
                }
            }

            callback.run();
        } else {
            OffenceData offenceData = new OffenceData();
            offenceData.setPunishment(punishmentType);
            offenceData.setPardonedBy(staff.getName());

            OffencesHandler handler = new OffencesHandler();
            handler.setUuid(offender.getUuid());
            handler.setOffenceData(offenceData);
            handler.send(Action.DELETE);
            handler.setResponseConsumer(message -> {
                if (message == null || !message.isOk()) {
                    PlayersService.i().edit(offender.getUuid(), player -> {
                        for (OffenceData offence : player.getOffences()) {
                            if (offence.isStillPunished() && offence.getPunishment().equals(punishmentType)) {
                                offence.setPardonedBy(staff.getName());
                                offence.setPardonedOn(Instant.now());
                            }
                        }

                        callback.run();
                    });
                } else {
                    callback.run();
                }
            });
        }

        sendMessage(staff, offender.getName(), punishmentType, false);
    }

    private OffenceData buildOffenceData(PlayerModel punisher, OffenceType offenceType, Pair<PunishmentType, Integer> punishment) {
        OffenceData offenceData = new OffenceData();
        offenceData.setType(offenceType);
        offenceData.setPunishedBy(punisher.getName());
        offenceData.setPunishedOn(Instant.now());
        if (punishment != null) {
            offenceData.setPunishment(punishment.key());
            offenceData.setDurationMinutes(punishment.value());
        }

        return offenceData;
    }

    private void sendMessage(PlayerModel staff, String name, PunishmentType punishmentType, boolean punish) {
        String msg = ChatColor.GOLD + staff.getName() + " " + ChatColor.YELLOW
                + (punish ? "punished " : "pardoned ")
                + name
                + (punish ? " with" : " from")
                + " a "
                + punishmentType.toString().toLowerCase();
        ChatService.i().handleStaffChat(null, msg);

        ChatHandler handler = new ChatHandler();
        handler.setChatType(ChatHandler.ChatType.STAFF);
        handler.setMessage(msg);
        handler.send();
    }

    /**
     * Check if the player is currently banned, muted, etc
     *
     * @param playerModel The player in question
     * @param type        The type of the punishment to check for
     * @return True if currently has this punishment active
     */
    public boolean isCurrentlyPunishedWith(PlayerModel playerModel, PunishmentType type) {
        return playerModel.getOffences().stream()
                .filter(o -> o.getPunishment().equals(type))
                .anyMatch(OffenceData::isStillPunished);
    }

    /**
     * Get a suggestion for the punishment
     *
     * @param playerModel Player to be punished
     * @param offenceType Offence type
     * @return The suggested punishment and its duration
     */
    public Pair<PunishmentType, Integer> getSuggestion(PlayerModel playerModel, OffenceType offenceType) {
        return offenceType.getSuggestion().apply(getCount(playerModel, offenceType) + 1);
    }

    /**
     * Count the number of offences this player has.
     * Only count the ones the player was punished for
     *
     * @param playerModel The player
     * @param offenceType The offence type
     * @return Number of offences
     */
    public int getCount(PlayerModel playerModel, OffenceType offenceType) {
        return (int) playerModel.getOffences().stream()
                .filter(o -> o.getPunishment() != null && o.getType().equals(offenceType))
                .count();
    }

    /**
     * Get the highest applicable and still punishable offence data
     *
     * @param playerModel    The player data
     * @param punishmentType The punishment type
     * @return The highest offence data or null
     */
    public OffenceData getHighestOffence(PlayerModel playerModel, PunishmentType punishmentType) {
        return playerModel.getOffences().stream()
                .filter(offence -> offence.getPunishment().equals(punishmentType) && offence.isStillPunished())
                .max(Comparator
                        .comparing(OffenceData::isPermanent)
                        .thenComparing(offence -> offence.getPunishedOn().plus(offence.getDurationMinutes(), ChronoUnit.MINUTES))
                )
                .orElse(null);
    }

    /**
     * Get the instant when the player's punishment (if any) will expire
     *
     * @param playerModel    The player
     * @param punishmentType The offence type
     * @return Instant of when it expires or null
     */
    public Instant getExpiresOn(PlayerModel playerModel, PunishmentType punishmentType) {
        OffenceData highest = getHighestOffence(playerModel, punishmentType);

        if (highest != null) {
            return highest.getPunishedOn().plus(highest.getDurationMinutes(), ChronoUnit.MINUTES);
        }

        return null;
    }
}
