package net.mineclick.global.service;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.events.PacketContainer;
import com.comphenix.protocol.wrappers.EnumWrappers;
import com.comphenix.protocol.wrappers.PlayerInfoData;
import com.comphenix.protocol.wrappers.WrappedChatComponent;
import com.comphenix.protocol.wrappers.WrappedGameProfile;
import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;
import net.mineclick.global.GlobalPlugin;
import net.mineclick.global.events.PlayerJoin;
import net.mineclick.global.messenger.OnlinePlayersHandler;
import net.mineclick.global.model.PlayerId;
import net.mineclick.global.util.Runner;
import net.mineclick.global.util.SingletonInit;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@SingletonInit
public class PlayerListService implements Listener {
    private static PlayerListService i;

    private List<PlayerId> onlinePlayers = new ArrayList<>();

    private PlayerListService() {
        Bukkit.getPluginManager().registerEvents(this, GlobalPlugin.i());
        Runner.async(0, 100, state -> updateTab());
    }

    public static PlayerListService i() {
        return i == null ? i = new PlayerListService() : i;
    }

    private void updateTab() {
        OnlinePlayersHandler.get(players -> {
            if (players == null)
                return;

            List<PlayerId> toAdd = new ArrayList<>(players);
            toAdd.removeAll(onlinePlayers);

            List<PlayerId> toRemove = new ArrayList<>(onlinePlayers);
            toRemove.removeAll(players);

            List<PlayerId> toUpdate = new ArrayList<>(players);
            toUpdate.removeAll(toAdd);

            removePlayers(toRemove);

            if (!toAdd.isEmpty()) {
                send(toAdd, EnumWrappers.PlayerInfoAction.ADD_PLAYER);
            }
            if (!toUpdate.isEmpty()) {
                send(toUpdate, EnumWrappers.PlayerInfoAction.UPDATE_LATENCY);
            }

            onlinePlayers = players;
        });
    }

    /**
     * @return all the players currently online on the network
     */
    public List<PlayerId> getOnlinePlayers() {
        return new ArrayList<>(onlinePlayers);
    }

    /**
     * @return all the currently online players' names
     */
    public List<String> getOnlineNames() {
        return getOnlineNames(stringMapEntry -> true);
    }

    /**
     * @param filter The predicate
     * @return all the currently online players' names
     */
    public List<String> getOnlineNames(Predicate<PlayerId> filter) {
        return onlinePlayers.stream()
                .filter(filter)
                .map(PlayerId::getName)
                .collect(Collectors.toList());
    }

    /**
     * Return a list of player names online on the network that start with the provided buffer
     *
     * @param buffer The start of the player's name
     * @return All the players that start with the provided string
     */
    public List<String> getPlayerListBasedOn(String buffer) {
        if (buffer.isEmpty()) {
            return onlinePlayers.stream().map(PlayerId::getName).collect(Collectors.toList());
        }

        return onlinePlayers.stream()
                .map(PlayerId::getName)
                .filter(p -> p.toLowerCase().startsWith(buffer.toLowerCase()))
                .collect(Collectors.toList());
    }

    private void send(List<PlayerId> players, EnumWrappers.PlayerInfoAction action) {
        if (players.isEmpty()) return;

        PacketContainer packet = new PacketContainer(PacketType.Play.Server.PLAYER_INFO);
        packet.getPlayerInfoActions().write(0, EnumSet.of(action));

        List<PlayerInfoData> list = players.stream().map(playerId -> {
            GameProfile profile = new GameProfile(playerId.getUuid(), playerId.getName());
            profile.getProperties().put("textures", new Property("textures", playerId.getTexture(), playerId.getSignature()));

            return new PlayerInfoData(WrappedGameProfile.fromHandle(profile), playerId.getPing(), EnumWrappers.NativeGameMode.ADVENTURE, WrappedChatComponent.fromText(playerId.getRank().getPrefix() + playerId.getName()));
        }).collect(Collectors.toList());
        packet.getPlayerInfoDataLists().write(1, list);

        PlayersService.i().forAll(p -> ProtocolLibrary.getProtocolManager().sendServerPacket(p.getPlayer(), packet));
    }

    private void removePlayers(List<PlayerId> players) {
        if (players.isEmpty()) return;

        PacketContainer packet = new PacketContainer(PacketType.Play.Server.PLAYER_INFO_REMOVE);
        packet.getUUIDLists().write(0, players.stream().map(PlayerId::getUuid).collect(Collectors.toList()));

        PlayersService.i().forAll(p -> ProtocolLibrary.getProtocolManager().sendServerPacket(p.getPlayer(), packet));
    }

    @EventHandler
    public void on(PlayerJoin e) {
        onlinePlayers.add(e.getPlayer());

        send(onlinePlayers, EnumWrappers.PlayerInfoAction.ADD_PLAYER);
    }
}
