package net.mineclick.global.service;

import net.mineclick.global.util.SingletonInit;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

@SingletonInit
public class VotesService {
    private static VotesService i;

    public static VotesService i() {
        return i == null ? i = new VotesService() : i;
    }

    /**
     * Handle player voting
     *
     * @param name Name of the player
     * @return True if player was found on this server
     */
    public boolean handleVote(String name) {
        Player player = Bukkit.getPlayerExact(name);
        if (player != null) {
            PlayersService.i().get(player.getUniqueId(), playerModel -> playerModel.processVotes(1));

            return true;
        }

        return false;
    }
}
