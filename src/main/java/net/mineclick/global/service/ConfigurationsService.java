package net.mineclick.global.service;

import lombok.SneakyThrows;
import net.mineclick.core.messenger.Action;
import net.mineclick.global.GlobalPlugin;
import net.mineclick.global.messenger.SettingsHandler;
import net.mineclick.global.model.ConfigurationsModel;
import net.mineclick.global.util.Runner;
import net.mineclick.global.util.SingletonInit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

@SingletonInit
public class ConfigurationsService {
    private static ConfigurationsService i;
    private final Map<String, Set<Runnable>> onUpdateRunnables = new HashMap<>();
    private YamlConfiguration oldConfigurations = new YamlConfiguration();
    private YamlConfiguration configurations = new YamlConfiguration();

    private ConfigurationsService() {
        Runner.sync(10, 200, state -> {
            SettingsHandler handler = new SettingsHandler();
            handler.setResponseConsumer(message -> {
                if (message != null) {
                    ConfigurationsModel configurations = ((SettingsHandler) message).getConfigurations();
                    if (configurations != null && configurations.getConfigurations() != null) {
                        state.cancel();

                        update(configurations.getConfigurations());
                        return;
                    }
                }

                GlobalPlugin.i().getLogger().severe("Configurations cannot be loaded!");
            });
            handler.send(Action.GET);
        });
    }

    public static ConfigurationsService i() {
        return i == null ? i = new ConfigurationsService() : i;
    }

    /**
     * Check if the configuration object was changed
     *
     * @param key The configuration key
     * @return True if different from the old configuration
     */
    public boolean hasChanged(String key) {
        if (oldConfigurations.contains(key) && configurations.contains(key)) {
            return !oldConfigurations.get(key).equals(configurations.get(key));
        }

        return configurations.contains(key);
    }

    /**
     * Update configurations
     *
     * @param configurations The new configurations
     */
    @SneakyThrows
    public void update(String configurations) {
        GlobalPlugin.i().getLogger().info("Configurations updating...");

        oldConfigurations = this.configurations;
        this.configurations = new YamlConfiguration();
        this.configurations.loadFromString(configurations);

        onUpdateRunnables.entrySet().stream()
                .filter(entry -> entry.getKey().equals("*") || hasChanged(entry.getKey()))
                .flatMap(entry -> {
                    GlobalPlugin.i().getLogger().info("Updating config for " + entry.getKey());
                    return entry.getValue().stream();
                })
                .forEach(Runnable::run);

        GlobalPlugin.i().getLogger().info("Configurations updated");
    }

    /**
     * Register an "on update" runnable.
     * It will be cached and called every time the configurations are updated
     *
     * @param runnable The runnable
     */
    public void onUpdate(Runnable runnable) {
        onUpdate("*", runnable);
    }

    /**
     * Register an "on update" runnable.
     * It will be cached and called every time the configurations value
     * is changed by the provided key
     *
     * @param key      The key to check by the configurations
     * @param runnable The runnable
     */
    public void onUpdate(String key, Runnable runnable) {
        onUpdateRunnables.computeIfAbsent(key, k -> new HashSet<>()).add(runnable);
    }

    /**
     * Check if the configurations contain the provided key
     *
     * @param key The key
     * @return True if the value exists under the key
     */
    public boolean contains(String key) {
        return configurations.contains(key);
    }

    /**
     * Get the configurations
     *
     * @return The configurations
     */
    public ConfigurationSection get() {
        return configurations;
    }

    /**
     * Get the configurations section by key
     *
     * @param key the section key
     * @return The configuration section
     */
    public ConfigurationSection get(String key) {
        return configurations.getConfigurationSection(key);
    }
}
