package net.mineclick.global.service;

import io.sentry.Sentry;
import net.mineclick.global.Constants;
import net.mineclick.global.GlobalPlugin;
import net.mineclick.global.util.SingletonInit;
import org.bukkit.Bukkit;

import java.util.logging.Handler;
import java.util.logging.LogRecord;
import java.util.stream.Collectors;

@SingletonInit
public class ExceptionService extends Handler {
    private static ExceptionService i;

    private ExceptionService() {
        if (Constants.SENTRY_URL != null && !Constants.SENTRY_URL.isEmpty()) {
            Sentry.init(Constants.SENTRY_URL);
        }
    }

    public static ExceptionService i() {
        return i == null ? i = new ExceptionService() : i;
    }

    @Override
    public void publish(LogRecord record) {
        if (ServersService.i().isTestNet() || record.getThrown() == null || !Sentry.isEnabled()) {
            return;
        }

        Throwable thrown = record.getThrown().getCause() != null ? record.getThrown().getCause() : record.getThrown();

        Sentry.setTag("server", GlobalPlugin.i().getServerId());
        Sentry.setTag("playersCount", String.valueOf(Bukkit.getOnlinePlayers().size()));
        Sentry.setTag("startedOn", String.valueOf(ServersService.i().getStartedOn()));
        Sentry.setTag("status", String.valueOf(ServersService.i().getStatus()));
        Sentry.setExtra("players",
                Bukkit.getOnlinePlayers().stream().map(p -> p.getName() + ":" + p.getUniqueId()).collect(Collectors.joining("\n")));
        Sentry.captureException(thrown);
    }

    @Override
    public void flush() {
    }

    @Override
    public void close() throws SecurityException {
        Sentry.close();
    }
}
