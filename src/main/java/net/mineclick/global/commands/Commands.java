package net.mineclick.global.commands;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import lombok.Builder;
import lombok.Getter;
import lombok.Singular;
import net.mineclick.global.GlobalPlugin;
import net.mineclick.global.model.PlayerModel;
import net.mineclick.global.service.PlayerListService;
import net.mineclick.global.service.PlayersService;
import net.mineclick.global.type.Rank;
import org.apache.commons.lang.RandomStringUtils;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.defaults.BukkitCommand;
import org.bukkit.craftbukkit.v1_20_R1.CraftServer;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.BiFunction;

public class Commands {
    public static final List<Command> commands = new ArrayList<>();
    private static final Cache<String, BiFunction<PlayerModel, String[], Boolean>> secretCommands = CacheBuilder.newBuilder()
            .expireAfterWrite(20, TimeUnit.MINUTES)
            .build();
    private static final Set<String> usedSecretCommands = new HashSet<>();

    /**
     * All commands expire in 20 minutes
     *
     * @param function Player that ran the command, arguments they provided and whether the command is single-use
     * @return The command
     */
    public static String makeSecretCommand(BiFunction<PlayerModel, String[], Boolean> function) {
        String command;
        do {
            command = "?mc" + RandomStringUtils.randomAlphabetic(4).toLowerCase();
        } while (usedSecretCommands.contains(command));
        usedSecretCommands.add(command);
        secretCommands.put(command, function);
        return command;
    }

    public static void addCommand(Command command) {
        commands.add(command);

        ((CraftServer) GlobalPlugin.i().getServer()).getCommandMap().register(command.name, new BukkitCommand(command.name) {
            @Override
            public boolean execute(CommandSender commandSender, String s, String[] strings) {
                return false;
            }

            @Override
            public boolean testPermissionSilent(CommandSender sender) {
                AtomicBoolean canSee = new AtomicBoolean();
                if (command.isHidden()) {
                    canSee.set(false);
                } else if (sender instanceof Player) {
                    PlayersService.i().get(((Player) sender).getUniqueId(), playerModel ->
                            canSee.set(playerModel.getRank().isAtLeast(command.getMinRank())));
                }

                return canSee.get();
            }

            @Override
            public List<String> tabComplete(CommandSender sender, String alias, String[] args) throws IllegalArgumentException {
                if (!(sender instanceof Player)) {
                    return new ArrayList<>();
                }

                AtomicReference<List<String>> completions = new AtomicReference<>(null);
                PlayersService.i().get(((Player) sender).getUniqueId(), playerModel -> {
                    if (playerModel.getRank().isAtLeast(command.getMinRank())) {
                        completions.set(PlayerListService.i().getPlayerListBasedOn(args[args.length - 1]));
                    }
                });

                return completions.get();
            }
        });
    }

    public static void process(String command, PlayerModel playerModel, String... args) {
        Player player = playerModel.getPlayer();
        if (player == null)
            return;

        BiFunction<PlayerModel, String[], Boolean> secretCommand = secretCommands.getIfPresent(command);
        if (secretCommand != null) {
            Boolean apply = secretCommand.apply(playerModel, args);
            if (apply) {
                secretCommands.invalidate(command);
            }
            return;
        }

        if (command.startsWith("?mc")) {
            playerModel.getPlayer().sendMessage(ChatColor.RED + "This action has expired");
            return;
        }

        Command cmd = get(command);
        if (cmd != null && playerModel.getRank().isAtLeast(cmd.getMinRank())) {
            if (args.length < cmd.minArgs) {
                playerModel.getPlayer().sendMessage(ChatColor.RED + "Command usage: " + ChatColor.GRAY + "/" + cmd.name + " " + cmd.usage);
                return;
            }

            String returnMsg = cmd.callFunction.apply(playerModel, args);
            if (returnMsg != null && !returnMsg.isEmpty()) {
                playerModel.getPlayer().sendMessage(returnMsg);
            }
        } else {
            if (!playerModel.getRank().isAtLeast(Rank.STAFF)) {
                playerModel.getPlayer().sendMessage(ChatColor.RED + "No such command. Maybe try /help?");
            }
        }
    }

    public static Command get(String cmd) {
        return commands.stream().filter(c -> c.name.equalsIgnoreCase(cmd) || c.aliases.contains(cmd.toLowerCase())).findAny().orElse(null);
    }

    public static boolean has(String command) {
        return get(command) != null || secretCommands.asMap().containsKey(command);
    }

    @Builder
    @Getter
    public static class Command {
        private String name;
        @Singular("alias")
        private List<String> aliases;
        @Builder.Default
        private String description = "";
        @Builder.Default
        private Rank minRank = Rank.DEFAULT;
        private BiFunction<PlayerModel, String[], String> callFunction;
        @Builder.Default
        private String usage = "";
        @Builder.Default
        private int minArgs = 0;
        private boolean playersTabComplete;
        private boolean hidden;
    }
}
