package net.mineclick.global.commands;

import net.mineclick.global.model.PlayerId;
import net.mineclick.global.model.ServerModel;
import net.mineclick.global.service.PlayerListService;
import net.mineclick.global.service.PlayersService;
import net.mineclick.global.service.ServersService;
import net.mineclick.global.service.TpService;
import net.mineclick.global.type.Rank;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;

public class StaffCommands {
    public static void load() {
        Commands.addCommand(Commands.Command.builder()
                .name("check")
                .description("Check if player is autoclicking")
                .usage("<playerName>")
                .minArgs(1)
                .minRank(Rank.STAFF)
                .playersTabComplete(true)
                .callFunction((data, strings) -> {
                    Player player = Bukkit.getPlayer(strings[0]);
                    if (player == null) {
                        return ChatColor.RED + "Can't find this player on the current server";
                    } else {
                        Location loc = player.getLocation();
                        loc.setPitch(-90);
                        player.teleport(loc);
                        return ChatColor.GOLD + "Moved " + player.getName() + "'s head up. Wait 15 seconds to see if they react and if not then create a report";
                    }
                })
                .build());

        Commands.addCommand(Commands.Command.builder()
                .name("gtp")
                .description("Teleport to a player (cross server)")
                .minRank(Rank.STAFF)
                .minArgs(1)
                .usage("<playerName>")
                .playersTabComplete(true)
                .callFunction((playerData, strings) -> {
                    String name = strings[0];

                    Player playerExact = Bukkit.getPlayerExact(name);
                    if (playerExact != null) {
                        PlayersService.i().hideFromAll(playerData);
                        playerData.getPlayer().teleport(playerExact);
                        return null;
                    }

                    PlayerId player = PlayerListService.i().getOnlinePlayers().stream()
                            .filter(p -> p.getName().equalsIgnoreCase(name))
                            .findFirst().orElse(null);
                    if (player == null) {
                        return "Player not on the network";
                    }

                    playerData.getOnJoinData().setTpToPlayer(name);
                    TpService.i().tpToServer(playerData, player.getServer());
                    return null;
                })
                .build());

        Commands.addCommand(Commands.Command.builder()
                .name("build")
                .description("Teleport to the build server")
                .minRank(Rank.STAFF)
                .callFunction((data, strings) -> {
                    for (ServerModel server : ServersService.i().getRunningServers(false)) {
                        // for now we just assume the non game server is the build server
                        if (!server.isGame()) {
                            TpService.i().tpToServer(data, server.getId());
                            return ChatColor.GOLD + "Teleporting...";
                        }
                    }

                    return ChatColor.RED + "No build servers found on this network. Make sure you're on staging or contact the dev";
                }).build());

        Commands.addCommand(Commands.Command.builder()
                .name("schat")
                .description("Toggle staff chat")
                .minRank(Rank.STAFF)
                .callFunction((data, strings) -> {
                    boolean toSet = !data.getChatData().isStaffChat();
                    data.getChatData().setStaffChat(toSet);

                    return ChatColor.YELLOW + "Staff chat is " + (toSet ? ChatColor.GREEN + "ON" : ChatColor.RED + "OFF");
                }).build());

        Commands.addCommand(Commands.Command.builder()
                .name("shutdown")
                .description("Gracefully shutdown the server")
                .minRank(Rank.DEV)
                .callFunction((data, strings) -> {
                    ServersService.i().gracefulShutdown();
                    return null;
                }).build());
    }
}
